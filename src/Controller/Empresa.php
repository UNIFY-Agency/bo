<?php

$loader->get('src/Model/Anuncios');
$loader->get('src/Model/Depoimentos');
$loader->get('src/Model/Documentos');
$loader->get('src/Model/Referencias');
$loader->get('src/Model/Categorias');
$loader->get('lib/Mail');

$anuncios = new Anuncios(new Config());
$publications = $anuncios->readAnuncios();

$depoimentos = new Depoimentos(new Config());
$depoiments = $depoimentos->readDepo($param);

$documentos = new Documentos(new Config());
$referencias = new Referencias(new Config());
$categorias = new Categorias(new Config());
$mail = new Mail();

$empresas = new Empresa(new Config());
$_SESSION['usuario']['empresa'] = $empresas->getEmpresa($_SESSION['usuario']['id']);

if($action == 'ver'){

  $company = $empresas->readUsuario(null, $param);
  $comp_dt = $empresas->companyData($param);

}

if($_SERVER['REQUEST_METHOD']=='POST' && $action == 'ver'){

    $_POST['empresa'] = $param;

    if(!$depoimentos->makeDepo($_POST)){
      echo "<script>alert('Você já fez um depoimento para essa empresa!');</script>";
    }

    header("Refresh:0");

}


if($_SERVER['REQUEST_METHOD']=='POST' && $action == 'index'){

  $documents = $_FILES['docs'];
  // $fields = array(0 => 'name' , 2 => 'type' , 3 => 'tmp_name' , 4 => 'error' , 5 => 'size');

  if(count($_POST) < 3){
    echo "<script>alert('Você deve enviar três referências');</script>";
    echo "<script>javascript:history.back(-1)</script>";
  }

  //montando array indicidual para cada arquivo
  for($c=0; $c <= count($documents); $c++){
    $docs[$c] = array_column($documents, $c);
  }
  //limpando indices vazios
  $docs = array_filter($docs);

  //setando tipos de arquivo permitidos
  $allowed =  array('gif','png' ,'jpg', 'pdf');

  //verificando a existencia de arquivos não permitidos
  foreach ($docs as $doc) {
    $ext = pathinfo($doc[0], PATHINFO_EXTENSION);
    if(!in_array($ext,$allowed) ) {
      echo "<script>alert('São permitidos apenas arquivos gif, png , jpg ou pdf. Você tentou enviar um arquivo ".$doc[1].".');</script>";
      echo "<script>javascript:history.back(-1)</script>";
      die();
    }
  }

  //montando array indicidual para cada referencia
  for($c=1; $c <= 3; $c++){
    $refs[$c]['nome'] = $_POST['nomeref'.$c];
    $refs[$c]['tele'] = preg_replace("/[^0-9]/", "", $_POST['telref'.$c]);
    $refs[$c]['mail'] = $_POST['emailref'.$c];
    $refs[$c]['serv'] = $_POST['servref'.$c];
  }
  //limpando indices vazios
  $refs = array_filter($refs);

  //upload de iamgens para pasta
  $c = 0;
  foreach ($docs as $doc) {
   	$docsFinal[$c] = $doc;
    array_push($docsFinal[$c], $_SESSION['usuario']['id']);
  	array_push($docsFinal[$c], $documentos->upFile($doc, $allowed));
    $only_num = preg_replace("/[^0-9]/", "", end($docsFinal[$c]));
    if(strlen($only_num) <= 3){
      echo "<script>alert('".end($docsFinal[$c])."');</script>";
      echo "<script>javascript:history.back(-1)</script>";
      die();
    }
  	$c++;
  }

  foreach ($docsFinal as $docFinal) {
    $documentos->insertDoc($docFinal);
  }

  foreach ($refs as $ref) {
    $referencias->insertRef($ref, $_SESSION['usuario']['id']);
  }

  //pegando os admins do sistema para enviar e-mail
  $admins = $users->readUsuario(null, null, null, true);
  //pegando dados da empresa logada agora
  $dados_comp = $empresas->getEmpresa($_SESSION['usuario']['id']);

  // enviando e-mail para cada ADM
  foreach ($admins as $admin) {
    $mail->companyDocsNotification($admin['email'], $dados_comp['nome']);
  }

  // echo '<pre>';
  // print_r($refs);
  // echo '</pre>';
  // echo '<pre>';
  // print_r($docsFinal);
  // echo '</pre>';

  echo "<script>alert('Todas as referências e documentos foram salvos com sucesso.');</script>";
  header("Refresh:0");

} else if($action == 'index'){

  $documents = $documentos->readDoc(null, null, $_SESSION['usuario']['id']);
  $referencs = $referencias->readRef(null, null, $_SESSION['usuario']['id']);

}

if($action == 'listar'){

    $companyes = $empresas->readUsuario(null, null, 2);

}
if($action == 'pendentes'){

    $companyes = $empresas->readUsuario(null, null, 2);

}
if($action == 'avaliar'){

    $company = $empresas->readUsuario($param);
    $dados_comp = $empresas->getEmpresa($param);
    $documents = $documentos->readDoc(null, null, $param);
    $referencs = $referencias->readRef(null, null, $param);

    if($_SERVER['REQUEST_METHOD']=='POST'){

      $empresas->comanyAprov($param);
      $mail->companyAprovNotification($company['email']);
      header('Location: ../pendentes');

    }

}

// echo '<pre>';
// print_r($admins);
// echo '</pre>';

?>
