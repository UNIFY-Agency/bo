<?php

header('Location: '.URL_BASE.'/produtos/listar');
die();

$loader->get('src/Model/Clientes');
$clientes = new Clientes(new Config());
$loader->get('src/Model/Pedidos');
$pedidos = new Pedidos(new Config());
$loader->get('src/Model/Acessos');
$acessos = new Acessos(new Config());

$mensagem_erro = false;

$clients = $clientes->clientCount()['total'];
$clients_lastweek = $clientes->clientLastWeek()['perc'];

$clientes->registrarCrescimentoDiario('RegClientsLastWeek', $clients_lastweek);

// $avarage_times = $pedidos->OrdersAvarageTime();
// $avarage_time = date('H:i:s', array_sum(array_map('strtotime', array_column($avarage_times, 'avarage'))) / count($avarage_times));

$orders_count = $pedidos->countAllOrders()['total'];
$orders_lastweek = $pedidos->ordersLastWeek()['perc'];

$clientes->registrarCrescimentoDiario('RegOrdersLastWeek', $orders_lastweek);

$canceled_orders_count = $pedidos->countAllCanceledOrders()['total'];
$canceled_orders_lastweek = $pedidos->CanceledOrdersLastWeek()['perc'];

$clientes->registrarCrescimentoDiario('RegCanceledOrdersLastWeek', $canceled_orders_lastweek);

$finalized_orders_count = $pedidos->countAllFinalizedOrders()['total'];
$finalized_orders_lastweek = $pedidos->FinalizedOrdersLastWeek()['perc'];

$clientes->registrarCrescimentoDiario('RegFinalizedOrdersLastWeek', $finalized_orders_lastweek);

$amount_count = $pedidos->countAmountAllOrders()['total'];
$amount_lastweek = $pedidos->amountLastWeek()['perc'];

$clientes->registrarCrescimentoDiario('RegAmountLastWeek', $amount_lastweek);

$os_rating = $acessos->ratingOS();
$rating_colors = array('blue','green','red','purple','aero', 'orange', 'yellow', 'brown', 'scarlet', 'silver');

// echo '<pre>';
// print_r($pedidos->OrdersAvarageTime());
// echo '</pre>';

?>
