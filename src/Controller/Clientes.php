<?php

  $clientes = new Clientes(new Config());
  $loader->get('src/Model/Pedidos');
  $pedidos = new Pedidos(new Config());
  $loader->get('src/Model/Categorias');
  $cat = new Categorias(new Config());
  $mensagem_erro = false;

if($action == 'listar'){

    $clients = $clientes->readUsuario();

} else if($action == 'inserir'){

  $categorias = $cat->readCat();

  if($_POST){
    if(!empty($_POST['nome']) and !empty($_POST['email']) and !empty($_POST['senha'])){

        $dados['nome'] = $_POST['nome'];
        $dados['email'] = strtolower($_POST['email']);
        $dados['senha'] = $users->hash($_POST['senha']);
        $dados['aniversario'] = empty($_POST['aniversario']) ? '0000-00-00' : date("Y-m-d", strtotime( str_replace('/', '-', $_POST['aniversario']) ) ) ;
        $dados['grupo'] = $_POST['categoria'];
        $dados['genero'] = is_null($_POST['gender']) ? NULL : $_POST['gender'] ;

        $result = $clientes->cadastrarCliente($dados);
        if(substr($result, 0, 4) == 'erro'){
          switch ($result) {
              case 'erro':
                $mensagem_erro = '<strong>Erro!</strong> Esse e-mail já está cadastrado no sistema!';
                break;
          }
        } else {
          header('Location: '.URL_BASE.'/clientes/listar');
        }
    } else {
      $mensagem_erro = '<strong>Erro!</strong> Algum dado obrigatório ficou em falta!';
    }
  }

  // echo '<pre>';
  // print_r($dados);
  // echo '</pre>';

} else if($action == 'editar'){

  if(empty($param)){ header('Location: '.URL_BASE.'/404'); }

  $cliente = $clientes->readUsuario(null, $param);
  $categorias = $cat->readCat();

  if($_POST){
    if(!empty($_POST['nome']) and !empty($_POST['email'])){

        $dados['nome'] = $_POST['nome'];
        $dados['email'] = strtolower($_POST['email']);
        $dados['senha'] = empty($_POST['senha']) ? 0 : $users->hash($_POST['senha']);
        $dados['aniversario'] = empty($_POST['aniversario']) ? '0000-00-00' : date("Y-m-d", strtotime( str_replace('/', '-', $_POST['aniversario']) ) ) ;
        $dados['grupo'] = $_POST['categoria'];
        $dados['genero'] = is_null($_POST['gender']) ? NULL : $_POST['gender'] ;

      $result = $clientes->updateUsuario($dados, $param);
      if(substr($result, 0, 4) == 'erro'){
        switch ($result) {
            case 'erro':
              $mensagem_erro = '<strong>Erro!</strong> Esse e-mail já está cadastrado no sistema!';
              break;
        }
      } else {
        header('Location: '.URL_BASE.'/clientes/listar');
      }

    }
  }

} else if($action == 'ver'){

  if(empty($param)){ header('Location: '.URL_BASE.'/404'); }

  $cliente = $clientes->readUsuario(null, $param);
  $cliente_orders = $pedidos->getUserOrdersDetails($cliente['id']);
  $order_status = array('Cancelado', 'Novo', 'Em Produção', 'Em Entrega', 'Concluído');

  // echo '<pre>';
  // print_r($cliente_orders);
  // echo '</pre>';

} else if($action == 'pesquisa'){

  if(!empty($_POST['email'])){

    $cliente = $clientes->buscarCliente($_POST['email']);

    if(empty($cliente)){
      $mensagem_erro = '<strong>Erro!</strong> Não existe nenhum cliente com esse e-mail cadastrado no sistema!';
    } else { header('Location: '.URL_BASE.'/clientes/ver/'.$cliente['id']); }

  }

}


if($param == 'del' && !empty($_POST)){

  $clientes->deleteUsuario($_POST['del-client-id']);
  header('Location: '.URL_BASE.'/clientes/listar');

  // echo '<pre>';
  // print_r($_POST);
  // echo '</pre>';

}

// echo '<pre>';
// print_r($clients);
// echo '</pre>';

?>
