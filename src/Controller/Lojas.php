<?php

$lojas = new Lojas(new Config());
$mensagem_erro = false;

if($action == 'listar'){

    $lojas_lista = $lojas->listarLojasMarca();

} else if($action == 'inserir'){

  $loader->get('src/Model/Marcas');
  $marcas = new Marcas(new Config());
  $marcas_lista = $marcas->listarMarcasPaises();
  $paises = $marcas->getPaisesAtivos();

  $marcas_combine = array();
  foreach ($marcas_lista as $element) {
      $marcas_combine[$element['id']][] = $element;
  }

  // echo '<pre>';
  // print_r($marcas_combine);
  // echo '</pre>';

  if(!empty($_POST['nome'])){

      $cat->insertCat($_POST);
      header('Location: listar');

  }

} else if($action == 'deletar'){

  $categoria = $cat->readCat($param);

  if($_SERVER['REQUEST_METHOD']=='POST'){

    $cat->deleteCat($param);
    header('Location: ../listar');

  }

} else if($action == 'editar'){

  $categoria = $cat->readCat($param);

  if(!empty($_POST['nome'])){

    $cat->editCat($_POST);
    header('Location: ../listar');

  }

}

// echo '<pre>';
// print_r($_POST);
// echo '</pre>';

?>
