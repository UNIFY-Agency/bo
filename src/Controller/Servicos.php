<?php

$loader->get('src/Model/Categorias');
$cat = new Categorias(new Config());

$serv = new Servicos(new Config());

if($action == 'listar'){

    $servicos = $serv->readServ();

} else if($action == 'inserir'){

  $categorias = $cat->readCat();

  if(!empty($_POST['nome'])){

      $serv->insertServ($_POST);
      header('Location: listar');

  }

} else if($action == 'deletar'){

  $servico = $serv->readServ($param);

  if($_SERVER['REQUEST_METHOD']=='POST'){

    $serv->deleteServ($param);
    header('Location: ../listar');

  }

} else if($action == 'editar'){

  $categorias = $cat->readCat();
  $servico = $serv->readServ($param);

  if(!empty($_POST['nome'])){

    $serv->editServ($_POST);
    header('Location: ../listar');

  }

}

// echo '<pre>';
// print_r($_POST);
// echo '</pre>';

?>
