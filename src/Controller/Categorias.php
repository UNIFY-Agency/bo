<?php

$cat = new Categorias(new Config());

if($action == 'listar'){

    $categorias = $cat->readCat();

} else if($action == 'inserir'){

  if(!empty($_POST['nome'])){

      $cat->insertCat($_POST);
      header('Location: '.URL_BASE.'/categorias/listar');

  }

} else if($action == 'deletar'){

  $categoria = $cat->readCat($param);

  if($_SERVER['REQUEST_METHOD']=='POST'){

    $cat->deleteCat($param);
    header('Location: ../listar');

  }

} else if($action == 'editar'){

  $categoria = $cat->readCat($param);

  if(!empty($_POST['nome'])){

    $cat->editCat($_POST);
    header('Location: ../listar');

  }

}

// echo '<pre>';
// print_r($_POST);
// echo '</pre>';

?>
