<?php

setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');

$loader->get('src/Model/Categorias');
$cat = new Categorias(new Config());

$loader->get('src/Model/Servicos');
$serv = new Servicos(new Config());

$loader->get('src/Model/Lances');
$lances = new Lances(new Config());

$loader->get('src/Model/Mensagens');
$mensagens = new Mensagens(new Config());

$loader->get('src/Model/Empresa');
$empresas = new Empresa(new Config());

$anuncios = new Anuncios(new Config());

if($action == 'listar'){

    $publications = ($_SESSION['usuario']['tipo'] == 0) ? $anuncios->readAnuncios() : $anuncios->readAnuncios(null, null, $_SESSION['usuario']['id']);

} else if($action == 'inserir'){

  $categorias = $cat->readCat();

  if(!empty($_POST['titulo']) and !empty($_POST['categoria'])){

      $_POST['local2cep'] = (empty($_POST['local2cep'])) ? 0 : $_POST['local2cep'] ;
      $_POST['cidade2']   = (empty($_POST['cidade2'])) ? '' : $_POST['cidade2'] ;
      $_POST['UF2']       = (empty($_POST['UF2'])) ? '' : $_POST['UF2'] ;
      $_POST['numero2']   = (empty($_POST['numero2'])) ? 0 : $_POST['numero2'] ;
      $_POST['complemento2'] = (empty($_POST['complemento2'])) ? 0 : $_POST['complemento2'] ;

      //ajeitando a data do dia do serviço
      $dataDia = explode('/', $_POST['diaServico']);
      $_POST['diaServico'] = $dataDia[2].'-'.$dataDia[1].'-'.$dataDia[0];

      //acertando a data final para receber lances
      $_POST['tempo'] = date('Y-m-d', strtotime('+'.($_POST['tempo'] * 30).' day'));

      $anuncios->cadastrarAnuncio($_POST);
      header('Location: listar');
      // echo "<script>window.location.replace('".URL_BASE."/anuncios/listar"."');<script>";
      // echo "<html><head><meta HTTP-EQUIV='Refresh' CONTENT='0;URL=".URL_BASE."/anuncios/listar".">";
      // echo "<script>window.location.href = ('".URL_BASE."/anuncios/listar"."');<script>";
      // echo "<script>window.location = '".URL_BASE."/anuncios/listar"."';<script>";

  }

} else if($action == 'ver'){

  $google_maps = true;

  $publication = $anuncios->readAnuncios(null, $param);
  $lances_lista = $lances->readLances(null, null, null, $param);
  $messages = $mensagens->readQuests($param);

}

if($_SERVER['REQUEST_METHOD']=='POST' && $action == 'ver'  && @$_POST['valor']){

    $_POST['anuncio'] = $param;

    if(!$lances->makeLance($_POST)){
      echo "<script>alert('Já existe um lance seu neste anuncio!');</script>";
    }

    header("Refresh:0");

}

if($_SERVER['REQUEST_METHOD']=='POST' && $action == 'ver' && @$_POST['mensagem']){

    $_POST['anuncio'] = $param;

    if(!$mensagens->makeMessage($_POST)){
      echo "<script>alert('Espere que sua mensagem anterior tenha sido respondida!');</script>";
    }

    header("Refresh:0");

}

if($action == 'fechar'){

  $google_maps = true;

  $lance = $lances->readLances(null, $param);
  $publication = $anuncios->readAnuncios(null, $lance['anuncio']);
  $company = $empresas->readUsuario(null, $lance['pai']);
  $dados_comp = $empresas->companyData($company['id']);

  if (Request::isAjax()){

    $lances->putWait($param);
    $anuncios->complementAnuncio($param, $_POST['dados']);
    $_SESSION['pagamento_espera'] = $_POST['dados'];


  }

}

// echo '<pre>';
// print_r($_POST);
// echo '</pre>';
