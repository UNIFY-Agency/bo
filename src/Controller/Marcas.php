<?php

  $marcas = new Marcas(new Config());
  $mensagem_erro = false;

if($action == 'listar'){

    $marcas_lista = $marcas->listarMarcasPaises();

    $marcas_combine = array();
    foreach ($marcas_lista as $element) {
        $marcas_combine[$element['id']][] = $element;
    }

} else if($action == 'inserir'){

  $paises = $marcas->getPaisesAtivos();

  if($_POST){
    if(!empty($_POST['nome']) and !empty($_POST['segmento']) and !empty($_POST['codigo'])){

        $dados['nome'] = $_POST['nome'];
        $dados['segmento'] = $_POST['segmento'];
        $dados['codigo'] = strtoupper($_POST['codigo']);
        $dados['descricao'] = empty($_POST['descricao']) ? '' : $_POST['descricao'] ;

        $marca_id = $marcas->insertMarca($dados);
        if(substr($marca_id, 0, 4) == 'erro'){
          switch ($marca_id) {
              case 'erro':
                $mensagem_erro = '<strong>Erro!</strong> Esse código já está sendo usado por uma marca!';
                break;
          }
        } else {
          $marcas->cadastrarMarcaPais($marca_id, $key, $value);
          header('Location: '.URL_BASE.'/marcas/listar');
        }

    } else {
      $mensagem_erro = '<strong>Erro!</strong> Algum dado obrigatório ficou em falta!';
    }
  }

  // echo '<pre>';
  // print_r($dados);
  // echo '</pre>';

} else if($action == 'editar'){

  if(empty($param)){ header('Location: '.URL_BASE.'/404'); }

  $marca = $marcas->listarMarcaPaises($param);
  $paises = $marcas->getPaisesAtivos();

  if($_POST){
    if(!empty($_POST['nome']) and !empty($_POST['segmento']) and !empty($_POST['codigo'])){

        $dados['nome'] = $_POST['nome'];
        $dados['segmento'] = $_POST['segmento'];
        $dados['codigo'] = strtoupper($_POST['codigo']);
        $dados['descricao'] = empty($_POST['descricao']) ? '' : $_POST['descricao'] ;

        $dados['PAISES'] = array();
        foreach ($paises as $key => $value) {
            if (array_key_exists($value['id'], $_POST)){
              $dados['PAISES'][$value['id']] = $_POST[$value['id']] == 'on' ? 1 : 0 ;
            } else {
              $dados['PAISES'][$value['id']] = 0 ;
            }
        }

        $atualiza = $marcas->editMarca($param, $dados);
        if(substr($atualiza, 0, 4) == 'erro'){
          switch ($atualiza) {
              case 'erro':
                $mensagem_erro = '<strong>Erro!</strong> Esse código já está sendo usado por uma marca!';
                break;
          }
        } else {
          foreach ($dados['PAISES'] as $key => $value) {
            $marcas->atualizaMarcaPais($param, $key, $value);
          }
          header('Location: '.URL_BASE.'/marcas/listar');
        }

    } else {
      $mensagem_erro = '<strong>Erro!</strong> Algum dado obrigatório ficou em falta!';
    }
  }
} else if($action == 'ver'){

  if(empty($param)){ header('Location: '.URL_BASE.'/404'); }

  $marca = $marcas->readMarcas($param);
  $paises = array_column($marcas->getPaisesMarca($param), 'pais');

}

?>
