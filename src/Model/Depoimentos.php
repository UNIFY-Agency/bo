<?php

class Depoimentos{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    //CRUD

    public function cadastrarDepo($dados){

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $cadastra = $this->mysql->prepare('INSERT INTO depoimentos (pai, empresa, descricao, inclusao) VALUES ('.$_SESSION['usuario']['id'].', :empresa, :descricao, :inclusao);');
            $cadastra->bindValue(':empresa', $dados['empresa'], PDO::PARAM_STR);
            $cadastra->bindValue(':descricao', $dados['descricao'], PDO::PARAM_STR);
            $cadastra->bindValue(':inclusao', date('Y-m-d'), PDO::PARAM_STR);
            $cadastra->execute();
            //header('Location:index.php?pages=anuncio');
        }
    }

    public function makeDepo($dados){

          $select = $this->mysql->prepare('SELECT id FROM depoimentos WHERE `empresa` = :empresa AND pai = '.$_SESSION['usuario']['id'].';');
          $select->bindValue(':empresa', $dados['empresa'], PDO::PARAM_STR);
          $select->execute();
          $count = $select->rowCount();

          if($count > 0){ return false; } else {

            $cadastra = $this->mysql->prepare('INSERT INTO depoimentos (pai, empresa, descricao, inclusao, publicado) VALUES ('.$_SESSION['usuario']['id'].', :empresa, :descricao, :inclusao, 1);');
            $cadastra->bindValue(':empresa', $dados['empresa'], PDO::PARAM_STR);
            $cadastra->bindValue(':descricao', $dados['descricao'], PDO::PARAM_STR);
            $cadastra->bindValue(':inclusao', date('Y-m-d'), PDO::PARAM_STR);
            $cadastra->execute();

            return true;
          }
    }

    public function readDepo($empresa=null, $id=null, $pai=null){
      if(!empty($empresa)) {
            $select = $this->mysql->prepare('SELECT * FROM depoimentos WHERE empresa = :empresa AND publicado = 1;');
            $select->bindValue(':empresa', $empresa, PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll();
      } else if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM depoimentos WHERE id = :id AND publicado = 1;');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else if(!empty($pai)) {
            $select = $this->mysql->prepare('SELECT * FROM depoimentos WHERE pai = :id AND publicado = 1;');
            $select->bindValue(':id', $pai  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetchAll();
        }else {
            $select = $this->mysql->prepare('SELECT * FROM depoimentos WHERE 1 ORDER BY valor ASC;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function readPubAnuncio($nome=null, $id=null){
        if(!empty($nome)) {
            $select = $this->mysql->prepare('SELECT * FROM anuncios WHERE titulo = :nome AND ativ = 1 AND publicado = 1;');
            $select->bindValue(':nome', $nome, PDO::PARAM_STR);
            $select->execute();
            return $select->fetch();
        } else if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM anuncios WHERE id = :id AND ativ = 1 AND publicado = 1;');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else {
            $select = $this->mysql->prepare('SELECT * FROM anuncios WHERE ativ = 1 AND publicado = 1;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function deleteAnuncio($id){
        $delete = $this->mysql->prepare('UPDATE anuncios SET ativ = 0 WHERE id = :id;');
        $delete->bindValue(':id', $id, PDO::PARAM_INT);
        $delete->execute();

        $foto = $this->searchFoto($id);
        foreach($foto as $fotos){ unlink($fotos['local']); }

        $deletef = $this->mysql->prepare('DELETE FROM fotos WHERE id_pai = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}
