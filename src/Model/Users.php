<?php
/**
 * Created by PhpStorm.
 * User: renan
 * Date: 07/09/15
 * Time: 19:17
 */

class Users{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    public function login($dados=null){

        session_start();

        // if(empty($dados)){
        //     if(!empty($_COOKIE['aaz_uryh']) and !empty($_COOKIE['aaz_oijd'])) {
        //         $dados['usuario'] = base64_decode($_COOKIE['aaz_uryh']);
        //         $dados['senha'] = base64_decode($_COOKIE['aaz_oijd']);
        //     }
        // } else {
        //
        //     $this->lembrar($dados);
        //
        // }

        if(isset($dados['usuario']) and isset($dados['senha'])) {

            $usuario = $this->retUsuario($dados['usuario']);
            if (password_verify($dados['senha'], $usuario['password'])) {
              if($usuario['tipo'] == 0){
                $_SESSION['admin'] = $usuario;
                $_SESSION['admin']['nivel'] = 0;
                header('Location: ./inicio');
              } else if($usuario['tipo'] > 0){
                $_SESSION['admin'] = $usuario;
                $_SESSION['admin']['nivel'] = 1;
                header('Location: ./inicio');
              } else {
                  return 'erro2';
              }
            } else {
                return 'erro';
            }
        }

    }

    public function force_login($dados=null){

        session_start();

        if(isset($dados['id'])) {
            $_SESSION['usuario'] = $dados;
        }

    }

    public function setVerificaUsuario($id){

        $update = $this->mysql->prepare('UPDATE usuarios SET email_verificado = 1 WHERE id = :id;');
        $update->bindValue(':id', $id, PDO::PARAM_INT);
        return $update->execute();

    }

    public function logout(){

        session_start();
        session_unset();
        session_destroy();
        setcookie('aaz_uryh');
        setcookie('aaz_oijd');
        header('Location: '.URL_BASE.'/login');

    }

    public function protege(){

        session_start();
        if(empty($_SESSION['admin'])){
            $this::logout();
        }

    }

    public function lembrar($dados){

        $cookie = array(
            'usuario'=>base64_encode($dados['usuario']),
            'senha'=>base64_encode($dados['senha'])
        );

        setcookie('aaz_uryh', $cookie['usuario'], (time() + (15*24*3600)), $_SERVER['SERVER_NAME']);
        setcookie('aaz_oijd', $cookie['senha'], (time() + (15*24*3600)), $_SERVER['SERVER_NAME']);

    }

    public function hash($senha){

        return password_hash($senha, PASSWORD_BCRYPT, array('cost'=>12));

    }

    public function retUsuario($user){

        $usuario = $this->mysql->prepare('SELECT * FROM usuarios WHERE email = :usuario');
        $usuario->bindValue(':usuario', $user, PDO::PARAM_STR);
        $usuario->execute();
        return $usuario->fetch(PDO::FETCH_ASSOC);

    }

    public function confirmUsuario($crypt){

        $usuario = $this->mysql->prepare('SELECT * FROM usuarios WHERE MD5(id) = :id_crypt');
        $usuario->bindValue(':id_crypt', $crypt, PDO::PARAM_STR);
        $usuario->execute();
        return $usuario->fetch(PDO::FETCH_ASSOC);

    }

    public function adminLevel($id){

        $usuario = $this->mysql->prepare('SELECT nivel FROM admin WHERE user_id = :id AND ativo = 1');
        $usuario->bindValue(':id', $id, PDO::PARAM_INT);
        $usuario->execute();
        return $usuario->fetch(PDO::FETCH_ASSOC);

    }

    public function getFranchiseeStore($email){

        $usuario = $this->mysql->prepare('SELECT * FROM `lojas` WHERE email = :email');
        $usuario->bindValue(':email', $email, PDO::PARAM_STR);
        $usuario->execute();
        return $usuario->fetch(PDO::FETCH_ASSOC);

    }

    //CRUD

    public function cadastrarCliente($dados){

      if ($dados['grupo'] == 0) {
        $tipo = 1;
      } else {
        $tipo = 0;
      }

      $email = $dados['email'];
      $consulta = $this->mysql->prepare("SELECT * FROM usuarios WHERE email=?");
      $consulta->execute([$email]);
      $user = $consulta->fetch();
      if ($user) {
          return 'erro';
      } else {
        $cadastra = $this->mysql->prepare('INSERT into usuarios (nome, email, password, aniversario, genero, tipo, ativo, grupo, criado) VALUES (:nome, :email, :password, :aniversario, :genero, :tipo, 1, :grupo, :criado);');
        $cadastra->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $cadastra->bindValue(':email', $dados['email'], PDO::PARAM_STR);
        $cadastra->bindValue(':password', $dados['senha'], PDO::PARAM_STR);
        $cadastra->bindValue(':aniversario', $dados['aniversario'], PDO::PARAM_STR);
        $cadastra->bindValue(':genero', $dados['genero'], PDO::PARAM_INT);
        $cadastra->bindValue(':tipo', $tipo, PDO::PARAM_INT);
        $cadastra->bindValue(':grupo', $dados['grupo'], PDO::PARAM_INT);
        $cadastra->bindValue(':criado', date('Y-m-d'), PDO::PARAM_STR);
        $cadastra->execute();
      }
    }

    public function readUsuario($nome=null, $id=null, $tipo=null, $admin=null){
        if(!is_null($nome)) {
            $select = $this->mysql->prepare('SELECT * FROM usuarios WHERE nome = :nome;');
            $select->bindValue(':nome', $nome, PDO::PARAM_STR);
            $select->execute();
            return $select->fetch();
        } else if(!is_null($id)) {
            $select = $this->mysql->prepare('SELECT * FROM usuarios WHERE id = :id;');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else if(!is_null($tipo)) {
            $select = $this->mysql->prepare('SELECT * FROM usuarios WHERE tipo = :tipo AND deletado = 0;');
            $select->bindValue(':tipo', $tipo  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetchAll();
        } else if(!is_null($admin)) {
            $select = $this->mysql->prepare('SELECT * FROM usuarios WHERE tipo = 0;');
            $select->execute();
            return $select->fetchAll();
        } else {
            $select = $this->mysql->prepare('SELECT * FROM usuarios WHERE deletado = 0;');
            $select->execute();
            return $select->fetchAll(PDO::FETCH_ASSOC);
        }

        $select->execute();
        return $select->fetch();
    }


    public function updateUsuario($dados, $id){

      if ($dados['grupo'] == 0) {
        $tipo = 1;
      } else {
        $tipo = 0;
      }

      $email = $dados['email'];
      $consulta = $this->mysql->prepare("SELECT * FROM usuarios WHERE email=?");
      $consulta->execute([$email]);
      $user = $consulta->fetch();
      if ($user) {
          return 'erro';
      } else {
        if(empty($dados['senha'])){
          $update = $this->mysql->prepare('UPDATE usuarios SET nome = :nome, email = :email, aniversario = :aniversario, genero = :genero, tipo = :tipo, grupo = :grupo, atualizado = :atualizado WHERE id = :id;');
          $update->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
          $update->bindValue(':email', $dados['email'], PDO::PARAM_STR);
          $update->bindValue(':aniversario', $dados['aniversario'], PDO::PARAM_STR);
          $update->bindValue(':genero', $dados['genero'], PDO::PARAM_INT);
          $update->bindValue(':tipo', $tipo, PDO::PARAM_INT);
          $update->bindValue(':grupo', $dados['grupo'], PDO::PARAM_INT);
          $update->bindValue(':atualizado', date('Y-m-d'), PDO::PARAM_STR);
          $update->bindValue(':id', $id, PDO::PARAM_INT);
          return $update->execute();
        } else {
          $update = $this->mysql->prepare('UPDATE usuarios SET nome = :nome, email = :email, password = :password, aniversario = :aniversario, genero = :genero, tipo = :tipo, grupo = :grupo, atualizado = :atualizado WHERE id = :id;');
          $update->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
          $update->bindValue(':email', $dados['email'], PDO::PARAM_STR);
          $update->bindValue(':password', $dados['senha'], PDO::PARAM_STR);
          $update->bindValue(':aniversario', $dados['aniversario'], PDO::PARAM_STR);
          $update->bindValue(':genero', $dados['genero'], PDO::PARAM_INT);
          $update->bindValue(':tipo', $tipo, PDO::PARAM_INT);
          $update->bindValue(':grupo', $dados['grupo'], PDO::PARAM_INT);
          $update->bindValue(':atualizado', date('Y-m-d'), PDO::PARAM_STR);
          $update->bindValue(':id', $id, PDO::PARAM_INT);
          return $update->execute();
        }
      }
    }

    public function deleteUsuario($id){
        $delete = $this->mysql->prepare('UPDATE usuarios SET deletado = 1 WHERE id = :id;');
        $delete->bindValue(':id', $id, PDO::PARAM_INT);
        return $delete->execute();
    }

    // public function deleteUsuario($id){
    //     $delete = $this->mysql->prepare('DELETE FROM usuarios WHERE id = :id;');
    //     $delete->bindValue(':id', $id, PDO::PARAM_INT);
    //     return $delete->execute();
    // }


}
