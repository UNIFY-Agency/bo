<?php
/**
 * Created by PhpStorm.
 * User: renan
 * Date: 07/09/15
 * Time: 19:17
 */

class Empresa extends Users{

  public function companyData($id=null){
      $select = $this->mysql->prepare('SELECT * FROM dados_empresa WHERE pai = :id;');
      $select->bindValue(':id', $id, PDO::PARAM_INT);
      $select->execute();
      return $select->fetch();
  }

  public function getEmpresa($id){
      $select = $this->mysql->prepare('SELECT * FROM `dados_empresa` WHERE pai = :id');
      $select->bindValue(':id', $id  , PDO::PARAM_INT);
      $select->execute();
      return $select->fetch();
  }

  public function comanyAprov($id){
      $select = $this->mysql->prepare('UPDATE `dados_empresa` SET autorizado = 1 WHERE pai = :pai;');
      $select->bindValue(':pai', $id  , PDO::PARAM_INT);
      $select->execute();
      return $select->fetch();
  }

}
