<?php

class Pedidos{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    //status 1 = novo
    //status 2 = preparo
    //status 3 = entrega
    //status 4 = finalizado
    //status 0 = cancelado

    public function getOrdersCupom($pid){
      $select = $this->mysql->prepare('SELECT * FROM `pedidos_cupom` INNER JOIN cupom ON cupom.id = pedidos_cupom.id_cupom WHERE id_pedido = :pid');
      $select->bindValue(':pid', $pid, PDO::PARAM_INT);
      $select->execute();
      $cupom = $select->fetch(PDO::FETCH_ASSOC);

      if($cupom['tipo'] == 3){
        $select2 = $this->mysql->prepare('SELECT nome as nome_produto FROM `produtos` WHERE id = :pid');
        $select2->bindValue(':pid', $cupom['valor'], PDO::PARAM_INT);
        $select2->execute();
        $produto = $select2->fetch(PDO::FETCH_ASSOC);
        $cupom = array_merge($cupom, $produto);
      }

      return $cupom;
    }

    public function lastOrderId($lid=null){
      if(empty($lid)){
        $select = $this->mysql->prepare('SELECT MAX(id) as id FROM pedidos');
        $select->execute();
        return $select->fetch(PDO::FETCH_ASSOC);
      } else {
        $select = $this->mysql->prepare('SELECT MAX(id) as id FROM pedidos WHERE id_loja = :lid;');
        $select->bindValue(':lid', $lid, PDO::PARAM_INT);
        $select->execute();
        return $select->fetch(PDO::FETCH_ASSOC);
      }
    }

    public function amountLastWeek(){

      $select = $this->mysql->prepare('SELECT ROUND(SUM(p.valor) / t.total * 100,2) as perc FROM pedidos p, ( SELECT SUM(valor) as total FROM `pedidos` WHERE status = 4 ) t WHERE DATEDIFF(CURRENT_DATE,criado) <= 7 AND status = 4');
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);

    }

    public function OrdersAvarageTime(){

      $select = $this->mysql->prepare('SELECT fin.id_pedido, fin.hora as final,
                                      (SELECT hora FROM pedido_mudanca_status WHERE id_pedido = fin.id_pedido AND status = 1) as inicio,
                                      TIMEDIFF(fin.hora, (SELECT hora FROM pedido_mudanca_status WHERE id_pedido = fin.id_pedido AND status = 1)) as avarage
                                      FROM pedido_mudanca_status fin
                                      WHERE fin.status = 4');
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);

    }

    public function ordersLastWeek(){

      $select = $this->mysql->prepare('SELECT ROUND(count(p.id) / t.total * 100,2) as perc FROM pedidos p, ( SELECT count(*) as total from pedidos where status <> 0 ) t WHERE DATEDIFF(CURRENT_DATE,criado) <= 7 AND status <> 0');
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);

    }

    public function FinalizedOrdersLastWeek(){

      $select = $this->mysql->prepare('SELECT ROUND(count(p.id) / t.total * 100,2) as perc FROM pedidos p, ( SELECT count(*) as total from pedidos where status = 4 ) t WHERE DATEDIFF(CURRENT_DATE,criado) <= 7 AND status = 4');
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);

    }

    public function CanceledOrdersLastWeek(){

      $select = $this->mysql->prepare('SELECT ROUND(count(p.id) / t.total * 100,2) as perc FROM pedidos p, ( SELECT count(*) as total from pedidos ) t WHERE DATEDIFF(CURRENT_DATE,criado) <= 7 AND status = 0');
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);

    }

    public function countAmountAllOrders(){
      $select = $this->mysql->prepare('SELECT SUM(valor) as total FROM `pedidos` WHERE status = 4');
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function countAllOrders(){
      $select = $this->mysql->prepare('SELECT COUNT(*) as total FROM `pedidos` WHERE 1');
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function countAllFinalizedOrders(){
      $select = $this->mysql->prepare('SELECT COUNT(*) as total FROM `pedidos` WHERE status = 4');
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function countAllCanceledOrders(){
      $select = $this->mysql->prepare('SELECT COUNT(*) as total FROM `pedidos` WHERE status = 0');
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function daysLastOrder($uid){
      $select = $this->mysql->prepare('SELECT id,DATEDIFF(CURRENT_DATE,criado) as dias FROM pedidos WHERE DATEDIFF(CURRENT_DATE,pedidos.criado) AND id_user = :id_user ORDER BY id DESC');
      $select->bindValue(':id_user', $uid, PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function countUserProducts($uid){
      $select = $this->mysql->prepare('SELECT SUM(i.quantidade) as total FROM carrinho_item i
                                        INNER JOIN carrinho c ON i.id_carrinho = c.id
                                        WHERE c.id_cliente = :id_user');
      $select->bindValue(':id_user', $uid, PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function countUserOrders($uid){
      $select = $this->mysql->prepare('SELECT COUNT(*) as count FROM `pedidos` WHERE id_user = :id_user');
      $select->bindValue(':id_user', $uid, PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function getUserOrders($uid){
      $select = $this->mysql->prepare('SELECT * FROM `pedidos` WHERE id_user = :id_user');
      $select->bindValue(':id_user', $uid, PDO::PARAM_INT);
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getUserOrdersDetails($uid){
      $select = $this->mysql->prepare('SELECT p.*, e.rua, e.numero, e.complemento, cp.codigo_postal FROM pedidos p
                                      INNER JOIN enderecos_comp e ON p.id = e.id_encomenda
                                      INNER JOIN carrinho c ON p.id_cart = c.id
                                      INNER JOIN codigos_entrega cp ON c.id_cp = cp.id
                                      WHERE id_user = :id_user');
      $select->bindValue(':id_user', $uid, PDO::PARAM_INT);
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function changeOrderStatus($oid, $status, $msg=null, $agente=null){
      $select = $this->mysql->prepare('SELECT id, status FROM `pedidos` WHERE id = :id');
      $select->bindValue(':id', $oid, PDO::PARAM_INT);
      $select->execute();
      $order = $select->fetch(PDO::FETCH_ASSOC);
      if($order['status'] != 0){
        $update = $this->mysql->prepare('UPDATE pedidos SET status = :status, atualizado = :atualizado WHERE id = :id ');
        $update->bindValue(':status', $status, PDO::PARAM_INT);
        $update->bindValue(':atualizado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
        $update->bindValue(':id', $oid, PDO::PARAM_INT);
        $update->execute();
        $cadastra = $this->mysql->prepare('INSERT INTO `pedido_mudanca_status` (`id_pedido`, `tipo_mudanca`, `status`,`hora`) VALUES (:id_pedido, :tipo_mudanca, :status, :hora);');
        $cadastra->bindValue(':id_pedido', $oid, PDO::PARAM_INT);
        $cadastra->bindValue(':tipo_mudanca', $agente, PDO::PARAM_STR);
        $cadastra->bindValue(':status', $status, PDO::PARAM_INT);
        $cadastra->bindValue(':hora', date("Y-m-d H:i:s"), PDO::PARAM_STR);
        $cadastra->execute();
        if($status == 0){
          $cadastra = $this->mysql->prepare('INSERT INTO `cancelamentos` (`id_pedido`, `msg`, `criado`) VALUES (:id_pedido, :msg, :criado);');
          $cadastra->bindValue(':id_pedido', $oid, PDO::PARAM_INT);
          $cadastra->bindValue(':msg', $msg, PDO::PARAM_STR);
          $cadastra->bindValue(':criado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
          $cadastra->execute();
        }
      }
    }

    public function getOrdersDetails($pid){
      $select = $this->mysql->prepare('SELECT i.id, i.id_produto, pr.nome as product_name, pr.preco, i.quantidade, pr.grupo, u.nome as user_name, u.tel, u.tel2, u.email,
                                      u.nif, p.fatura, p.tipo_pag, p.troco, p.entrega, p.taxa_entrega, p.adiantado, p.valor as total_pedido, c.alergias, p.pontos, l.taxa_entrega as loja_taxa, e.taxa_entrega as cp_taxa, pr.image_url,
                                     (SELECT concat(freguesia, " - ", rua, " ", numero, " ", complemento) FROM enderecos_comp WHERE id_encomenda = p.id ) as endereco_comp, e.codigo_postal, 0 as combo FROM carrinho_item i
                                        INNER JOIN produtos pr ON i.id_produto = pr.id
                                        INNER JOIN carrinho c ON i.id_carrinho = c.id
                                        INNER JOIN codigos_entrega e ON c.id_cp = e.id
                                        INNER JOIN lojas l ON c.id_loja = l.id
                                        INNER JOIN pedidos p ON c.id_encomenda = p.id
                                        INNER JOIN usuarios u ON p.id_user = u.id
                                        WHERE i.removido = 0 AND p.id = :pid');
      $select->bindValue(':pid', $pid, PDO::PARAM_INT);
      $select->execute();
      //return $select->fetchAll(PDO::FETCH_ASSOC);
      $cart = $select->fetchAll(PDO::FETCH_ASSOC);
      foreach ($cart as $cart_item) {
        $final_cart[] = $cart_item;
        if($cart_item['grupo'] == 2){
          $c = 0;
          $combo_itens = $this->getCartGroupItens($cart_item['id']);
          foreach ($combo_itens as $cart_group_item) {
          $final_cart[] = $cart_group_item;
          $c++;
          }
        }
      }
      return $final_cart;
    }

    public function getCartGroupItens($lid){
      $select = $this->mysql->prepare('SELECT ci.id, p.id as id_produto, p.nome as product_name, 0 as preco, cgi.quantidade, cp.grupo, 0 as user_name, null as loja_taxa, null as cp_taxa,
                                      p.image_url, 0 as endereco_comp, 1 as combo FROM carrinho_group_item cgi
                                      INNER JOIN carrinho_item ci ON cgi.id_linha = ci.id
                                      INNER JOIN produtos p ON cgi.id_produto = p.id
                                      INNER JOIN produtos cp ON ci.id_produto = cp.id
                                      WHERE cgi.id_linha = :id_line');
      $select->bindValue(':id_line', $lid  , PDO::PARAM_INT);
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getOrdersByStatus($status, $lid=null){
      if(is_null($lid)){
        $select = $this->mysql->prepare('SELECT p.*, u.nome, u.email, e.codigo_postal FROM pedidos p
                                          INNER JOIN usuarios u ON p.id_user = u.id
                                          INNER JOIN carrinho c ON p.id_cart = c.id
                                          INNER JOIN codigos_entrega e ON c.id_cp = e.id WHERE p.status = :status ORDER BY id DESC');
        $select->bindValue(':status', $status, PDO::PARAM_INT);
        $select->execute();
        return $select->fetchAll(PDO::FETCH_ASSOC);
      } else {
        $select = $this->mysql->prepare('SELECT p.*, u.nome, u.email, e.codigo_postal FROM pedidos p
                                          INNER JOIN usuarios u ON p.id_user = u.id
                                          INNER JOIN carrinho c ON p.id_cart = c.id
                                          INNER JOIN codigos_entrega e ON c.id_cp = e.id WHERE p.status = :status AND p.id_loja = :id_loja ORDER BY id DESC');
        $select->bindValue(':status', $status, PDO::PARAM_INT);
        $select->bindValue(':id_loja', $lid, PDO::PARAM_INT);
        $select->execute();
        return $select->fetchAll(PDO::FETCH_ASSOC);
      }
    }

    public function sumUserPoints($uid){
      $select = $this->mysql->prepare('SELECT SUM(p.pontos) as points FROM `pedidos` p WHERE id_user = :id_user');
      $select->bindValue(':id_user', $uid, PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function createOrder($dados){
      $cadastra = $this->mysql->prepare('INSERT INTO `pedidos` (`referencia`, `id_cart`, `id_user`, `valor`, `status`, `pontos`, `criado`) VALUES (:referencia, :id_cart, :id_user, :valor, :status, :pontos, :criado);');
      $cadastra->bindValue(':referencia', $dados['ref'], PDO::PARAM_STR);
      $cadastra->bindValue(':id_cart', $dados['cid'], PDO::PARAM_INT);
      $cadastra->bindValue(':id_user', $dados['uid'], PDO::PARAM_INT);
      $cadastra->bindValue(':valor', $dados['total'], PDO::PARAM_STR);
      $cadastra->bindValue(':status', $dados['status'], PDO::PARAM_INT);
      $cadastra->bindValue(':pontos', $dados['pontos'], PDO::PARAM_STR);
      $cadastra->bindValue(':criado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
      $cadastra->execute();
      $id_encomenda = $this->mysql->lastInsertId();

      $update = $this->mysql->prepare('UPDATE `carrinho` SET `id_encomenda`= :id_encomenda WHERE id = :id;');
      $update->bindValue(':id_encomenda', $id_encomenda, PDO::PARAM_INT);
      $update->bindValue(':id', $dados['cid'], PDO::PARAM_INT);
      $update->execute();

      $cadastra2 = $this->mysql->prepare('INSERT INTO `enderecos_comp` (`id_encomenda`, `rua`, `numero`, `complemento`, `criado`) VALUES (:id_encomenda, :rua, :numero, :complemento, :criado);');
      $cadastra2->bindValue(':id_encomenda', $id_encomenda, PDO::PARAM_INT);
      $cadastra2->bindValue(':rua', $dados['rua'], PDO::PARAM_STR);
      $cadastra2->bindValue(':numero', $dados['numero'], PDO::PARAM_INT);
      $cadastra2->bindValue(':complemento', $dados['complemento'], PDO::PARAM_STR);
      $cadastra2->bindValue(':criado', date("Y-m-d"), PDO::PARAM_STR);
      $cadastra2->execute();
      return $id_encomenda;
    }

    public function getOrderById($oid){
      $select = $this->mysql->prepare('SELECT * FROM pedidos WHERE id = :id');
      $select->bindValue(':id', $oid  , PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function getOrderByRef($ref){
      $select = $this->mysql->prepare('SELECT * FROM pedidos WHERE referencia = :referencia');
      $select->bindValue(':referencia', $ref  , PDO::PARAM_STR);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function getOrderByStore($lid){
      $select = $this->mysql->prepare('SELECT * FROM pedidos WHERE id_loja = :id_loja');
      $select->bindValue(':id_loja', $lid , PDO::PARAM_INT);
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getOrderByStores($lids_str){
      $query = "SELECT * FROM pedidos WHERE id_loja IN($lids_str)";
      $select = $this->mysql->prepare($query);
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getOrderByStoresCronAutoAdvance($lids_str){
      //$query = "SELECT * FROM pedidos WHERE id_loja IN($lids_str) AND status IN(1,2,3)";
      $query = "SELECT * FROM pedidos WHERE id_loja IN($lids_str) AND status IN(2,3)";
      $select = $this->mysql->prepare($query);
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
      //return $select->debugDumpParams();
    }

    public function countOrdersLastMinute(){
      $select = $this->mysql->prepare('SELECT COUNT(*) as count FROM `pedidos` WHERE `criado` >= :init AND `criado` < :final');
      $select->bindValue(':init', date('Y-m-d H:i'), PDO::PARAM_STR);
      $select->bindValue(':final', date('Y-m-d H:i', strtotime('+1 minute')), PDO::PARAM_STR);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function LastOrder(){
      $select = $this->mysql->prepare('SELECT id, referencia FROM pedidos ORDER BY id DESC LIMIT 1');
      $select->bindValue(':id', $oid  , PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function buildOrderReference($getLojaMarca){

      $letras = range('A', 'Z');

      $grupo = 'R';
      $marca = $getLojaMarca['marca_codigo'];
      $pais = $getLojaMarca['pais'];
      $loja = sprintf('%03d', $getLojaMarca['codigo']);
      $data = date('dmy');
      $hora = date('Hi');

      $incomplete_order_ref = $grupo.$marca.$pais.$loja.$data.$hora;
      $last_orders = $this->countOrdersLastMinute()['count'];
      $letter_key = $last_orders == 0 ? 0 : ceil($last_orders / 100) - 1;

      if($last_orders % 100 == 0){
      	$letter_key = $last_orders >= 100 ? $letter_key +1 : $letter_key ;
      	$complete_order_ref = $incomplete_order_ref.$letras[$letter_key].'00';
        return $complete_order_ref;
      } else {
      	$position = sprintf('%02d', substr($last_orders, -2));
      	$complete_order_ref = $incomplete_order_ref.$letras[$letter_key].$position;
        return $complete_order_ref;
      }

    }

    //CRUD

    public function insertCat($dados){

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $cadastra = $this->mysql->prepare('INSERT INTO produtos_cat (nome) VALUES (:nome);');
            $cadastra->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
            $cadastra->execute();
        }
    }

    public function readCat($id=null, $name=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE nome = :nome');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll();
        }else {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function editCat($dados){
        $deletef = $this->mysql->prepare('UPDATE produtos_cat SET nome = :nome WHERE id = :id ');
        $deletef->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $deletef->bindValue(':id', $dados['id'], PDO::PARAM_INT);
        $deletef->execute();
    }

    public function deleteCat($id){
        $deletef = $this->mysql->prepare('DELETE FROM produtos_cat WHERE id = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}
