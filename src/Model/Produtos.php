<?php

class Produtos{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    public function ativaProd($id){
        $delete = $this->mysql->prepare('UPDATE produtos SET ativo = 1 WHERE id = :id;');
        $delete->bindValue(':id', $id, PDO::PARAM_INT);
        return $delete->execute();
    }

    public function inativaProd($id){
        $delete = $this->mysql->prepare('UPDATE produtos SET ativo = 0 WHERE id = :id;');
        $delete->bindValue(':id', $id, PDO::PARAM_INT);
        return $delete->execute();
    }

    public function getProdutoMarca($pid){
      $select = $this->mysql->prepare('SELECT p.*, m.nome as marca FROM produtos p
                                        INNER JOIN marcas m ON p.id_marca = m.id
                                        WHERE p.id = :id');
      $select->bindValue(':id', $pid, PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function getUserFiltredProducts($grupo){

      if($grupo == 0){
        $select = $this->mysql->prepare('SELECT p.*, c.nome as cat FROM produtos p
                                          INNER JOIN produtos_cat c ON p.categoria = c.id
                                          WHERE 1');
        $select->execute();
        return $select->fetchAll(PDO::FETCH_ASSOC);
      } else {
        $select = $this->mysql->prepare("SELECT p.*, c.nome as cat FROM produtos p
                                          INNER JOIN produtos_cat c ON p.categoria = c.id
                                          WHERE p.categoria IN(1,$grupo)");
        $select->execute();
        return $select->fetchAll(PDO::FETCH_ASSOC);
      }
    }

    public function getAllProdutoCategoria(){
      $select = $this->mysql->prepare('SELECT p.*, c.nome as cat FROM produtos p
                                        INNER JOIN produtos_cat c ON p.categoria = c.id
                                        WHERE 1');
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getProdutoCategoria(){
      $select = $this->mysql->prepare('SELECT p.*, c.nome as cat FROM produtos p
                                        INNER JOIN produtos_cat c ON p.categoria = c.id
                                        WHERE p.ativo = 1');
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    //CRUD

    public function insertProd($dados){
      //$cadastra = $this->mysql->prepare('INSERT INTO produtos (nome, categoria, descricao, preco, ativo, image_url, id_marca, criado) VALUES (:nome, :categoria, :descricao, :preco, :ativo, :image_url, :id_marca, :criado);');
      $cadastra = $this->mysql->prepare('INSERT INTO produtos (referencia, nome, categoria, descricao, ativo, image_url, id_marca, criado) VALUES (:referencia, :nome, :categoria, :descricao, :ativo, :image_url, :id_marca, :criado);');
      $cadastra->bindValue(':referencia', $dados['referencia'], PDO::PARAM_STR);
      $cadastra->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
      $cadastra->bindValue(':categoria', $dados['categoria'], PDO::PARAM_STR);
      $cadastra->bindValue(':descricao', $dados['descricao'], PDO::PARAM_STR);
      //$cadastra->bindValue(':preco', $dados['preco'], PDO::PARAM_STR);
      $cadastra->bindValue(':ativo', 1, PDO::PARAM_INT);
      $cadastra->bindValue(':image_url', $dados['image_url'], PDO::PARAM_STR);
      $cadastra->bindValue(':id_marca', $dados['marca'], PDO::PARAM_INT);
      $cadastra->bindValue(':criado', date("Y-m-d"), PDO::PARAM_STR);
      $cadastra->execute();
    }

    public function readProd($id=null, $name=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM produtos WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch(PDO::FETCH_ASSOC);
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM produtos WHERE nome = :nome');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll(PDO::FETCH_ASSOC);
        }else {
            $select = $this->mysql->prepare('SELECT * FROM produtos WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll(PDO::FETCH_ASSOC);
        }

        $select->execute();
        return $select->fetch();
    }

    public function editProd($dados){
      if(empty($dados['image_url'])){
        $update = $this->mysql->prepare('UPDATE produtos SET nome=:nome, categoria=:categoria, descricao=:descricao, preco=:preco, ativo=:ativo, atualizado=:atualizado WHERE id = :id;');
        $update->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $update->bindValue(':categoria', $dados['categoria'], PDO::PARAM_STR);
        $update->bindValue(':descricao', $dados['descricao'], PDO::PARAM_STR);
        $update->bindValue(':preco', $dados['preco'], PDO::PARAM_STR);
        $update->bindValue(':ativo', 1, PDO::PARAM_INT);
        $update->bindValue(':atualizado', date("Y-m-d"), PDO::PARAM_STR);
        $update->bindValue(':id', $dados['id'], PDO::PARAM_INT);
        $update->execute();
      } else {
        $update = $this->mysql->prepare('UPDATE produtos SET nome=:nome, categoria=:categoria, descricao=:descricao, preco=:preco, ativo=:ativo, image_url=:image_url, atualizado=:atualizado WHERE id = :id;');
        $update->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $update->bindValue(':categoria', $dados['categoria'], PDO::PARAM_STR);
        $update->bindValue(':descricao', $dados['descricao'], PDO::PARAM_STR);
        $update->bindValue(':preco', $dados['preco'], PDO::PARAM_STR);
        $update->bindValue(':ativo', 1, PDO::PARAM_INT);
        $update->bindValue(':image_url', $dados['image_url'], PDO::PARAM_STR);
        $update->bindValue(':atualizado', date("Y-m-d"), PDO::PARAM_STR);
        $update->bindValue(':id', $dados['id'], PDO::PARAM_INT);
        $update->execute();
      }
    }

    public function deleteProd($id){
        $delete = $this->mysql->prepare('UPDATE produtos SET ativo = 0 WHERE id = :id;');
        $delete->bindValue(':id', $id, PDO::PARAM_INT);
        return $delete->execute();
    }


}
