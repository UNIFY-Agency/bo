<?php

class Enderecos{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    public function buscarCP($cp){
        $select = $this->mysql->prepare('SELECT * FROM codigos_entrega WHERE `codigo_postal` = :cp');
        $select->bindValue(':cp', $cp, PDO::PARAM_INT);
        $select->execute();
        return $select->fetch(PDO::FETCH_ASSOC);
    }

    //CRUD

    public function insertEndereco($dados){

      $dado = $dados['codigo_postal'];
      $consulta = $this->mysql->prepare("SELECT * FROM codigos_entrega WHERE codigo_postal=?");
      $consulta->execute([$dado]);
      $registro = $consulta->fetch();
      if ($registro) {
          return 'erro';
      } else {
        $cadastra = $this->mysql->prepare('INSERT INTO codigos_entrega (nome_rua, codigo_postal, taxa_entrega, id_loja, criado) VALUES (:nome_rua, :codigo_postal, :taxa_entrega, :id_loja, :criado);');
        $cadastra->bindValue(':nome_rua', $dados['nome_rua'], PDO::PARAM_STR);
        $cadastra->bindValue(':codigo_postal', $dados['codigo_postal'], PDO::PARAM_INT);
        $cadastra->bindValue(':taxa_entrega', $dados['taxa_entrega'], PDO::PARAM_STR);
        $cadastra->bindValue(':id_loja', $dados['id_loja'], PDO::PARAM_STR);
        $cadastra->bindValue(':criado', date('Y-m-d'), PDO::PARAM_STR);
        $cadastra->execute();
      }
    }

    public function readEnderecos($id=null, $name=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM codigos_entrega WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch(PDO::FETCH_ASSOC);
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM codigos_entrega WHERE nome = :nome');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll();
        }else {
            $select = $this->mysql->prepare('SELECT * FROM codigos_entrega WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function editEndereco($dados, $id){
      $consulta = $this->mysql->prepare("SELECT * FROM codigos_entrega WHERE codigo_postal=:codigo_postal AND id <> :id");
      $consulta->bindValue(':codigo_postal', $dados['codigo_postal'], PDO::PARAM_INT);
      $consulta->bindValue(':id', $id, PDO::PARAM_INT);
      $consulta->execute();
      $registro = $consulta->fetch();
      if ($registro) {
          return 'erro';
      } else {
        $atualiza = $this->mysql->prepare('UPDATE codigos_entrega SET nome_rua=:nome_rua, localidade=:localidade, codigo_postal=:codigo_postal, id_loja=:id_loja, taxa_entrega=:taxa_entrega, ativo=:ativo, atualizado=:atualizado WHERE id = :id ');
        $atualiza->bindValue(':nome_rua', $dados['nome_rua'], PDO::PARAM_STR);
        $atualiza->bindValue(':localidade', $dados['localidade'], PDO::PARAM_STR);
        $atualiza->bindValue(':codigo_postal', $dados['codigo_postal'], PDO::PARAM_INT);
        $atualiza->bindValue(':id_loja', $dados['id_loja'], PDO::PARAM_INT);
        $atualiza->bindValue(':taxa_entrega', $dados['taxa_entrega'], PDO::PARAM_STR);
        $atualiza->bindValue(':ativo', $dados['ativo'], PDO::PARAM_INT);
        $atualiza->bindValue(':atualizado', date('Y-m-d'), PDO::PARAM_STR);
        $atualiza->bindValue(':id', $id, PDO::PARAM_INT);
        $atualiza->execute();
      }
    }

    public function deleteCat($id){
        $deletef = $this->mysql->prepare('DELETE FROM produtos_cat WHERE id = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}
