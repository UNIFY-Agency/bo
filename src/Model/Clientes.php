<?php
/**
 * Created by PhpStorm.
 * User: renan
 * Date: 07/09/15
 * Time: 19:17
 */

class Clientes extends Users{

  public function clientCount(){

    $usuario = $this->mysql->prepare('SELECT COUNT(*) as total FROM usuarios WHERE tipo = 0');
    $usuario->execute();
    return $usuario->fetch(PDO::FETCH_ASSOC);

  }

  public function clientLastWeek(){
    //SELECT * FROM usuarios WHERE tipo = 0 AND criado >= curdate() - INTERVAL DAYOFWEEK(curdate())+6 DAY AND criado < curdate() - INTERVAL DAYOFWEEK(curdate())-1 DAY
    //SELECT count(u.id) / t.total * 100 as perc FROM usuarios u, ( SELECT count(*) as total from usuarios ) t WHERE DATEDIFF(CURRENT_DATE,criado) <= 7
    $usuario = $this->mysql->prepare('SELECT ROUND(count(u.id) / t.total * 100, 2) as perc FROM usuarios u, ( SELECT count(*) as total from usuarios ) t WHERE DATEDIFF(CURRENT_DATE,criado) <= 7');
    $usuario->execute();
    return $usuario->fetch(PDO::FETCH_ASSOC);

  }

  public function registrarCrescimentoDiario($nome, $porcentagem){

    $hoje = date('Y-m-d');
    $consulta = $this->mysql->prepare("SELECT * FROM porcentagens WHERE data = :data AND nome = :nome");
    $consulta->bindValue(':data', $hoje, PDO::PARAM_STR);
    $consulta->bindValue(':nome', $nome, PDO::PARAM_STR);
    $consulta->execute();
    $registro = $consulta->fetch();
    if ($registro) {
        return false;
    } else {
      $cadastra = $this->mysql->prepare('INSERT INTO `porcentagens`(`nome`, `porcentagem`, `data`) VALUES  (:nome, :porcentagem, :data);');
      $cadastra->bindValue(':nome', $nome, PDO::PARAM_STR);
      $cadastra->bindValue(':porcentagem', $porcentagem, PDO::PARAM_STR);
      $cadastra->bindValue(':data', $hoje, PDO::PARAM_STR);
      $cadastra->execute();
      return true;
    }
  }

  public function listarClientes(){
      $select = $this->mysql->prepare('SELECT u.id, u.nome, u.aniversario, u.email, u.tel2, u.criado,
                                      (SELECT COUNT(id) FROM pedidos WHERE id_user = u.id) total_pedidos
                                      FROM usuarios u WHERE u.tipo = 0 AND u.deletado = 0');
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
  }

  public function buscarCliente($email){
      $select = $this->mysql->prepare('SELECT * FROM usuarios WHERE tipo = 0 AND deletado = 0 AND email = :email');
      $select->bindValue(':email', $email, PDO::PARAM_STR);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
  }


}
