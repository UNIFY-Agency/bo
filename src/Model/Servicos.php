<?php

class Servicos{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    //CRUD

    public function insertServ($dados){

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $cadastra = $this->mysql->prepare('INSERT INTO servicos (nome, categoria) VALUES (:nome, :categoria);');
            $cadastra->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
            $cadastra->bindValue(':categoria', $dados['categoria'], PDO::PARAM_STR);
            $cadastra->execute();
        }
    }

    public function readServ($id=null, $name=null, $cat=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM servicos WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM servicos WHERE nome = :nome');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll();
        } else if(!empty($cat)) {
            $select = $this->mysql->prepare('SELECT * FROM servicos WHERE categoria = :categoria');
            $select->bindValue(':categoria', $cat  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll();
        }else {
            $select = $this->mysql->prepare('SELECT * FROM servicos WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function editServ($dados){
        $deletef = $this->mysql->prepare('UPDATE servicos SET nome = :nome, categoria = :categoria WHERE id = :id ');
        $deletef->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $deletef->bindValue(':categoria', $dados['categoria'], PDO::PARAM_STR);
        $deletef->bindValue(':id', $dados['id'], PDO::PARAM_INT);
        $deletef->execute();
    }

    public function deleteServ($id){
        $deletef = $this->mysql->prepare('DELETE FROM servicos WHERE id = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}
