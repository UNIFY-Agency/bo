<?php

class Mensagens{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    //CRUD

    public function cadastrarDepo($dados){

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $cadastra = $this->mysql->prepare('INSERT INTO depoimentos (pai, empresa, descricao, inclusao) VALUES ('.$_SESSION['usuario']['id'].', :empresa, :descricao, :inclusao);');
            $cadastra->bindValue(':empresa', $dados['empresa'], PDO::PARAM_STR);
            $cadastra->bindValue(':descricao', $dados['descricao'], PDO::PARAM_STR);
            $cadastra->bindValue(':inclusao', date('Y-m-d'), PDO::PARAM_STR);
            $cadastra->execute();
            //header('Location:index.php?pages=anuncio');
        }
    }

    public function makeMessage($dados){

          $select_last = $this->mysql->query('SELECT * FROM mensagens WHERE pai = '.$_SESSION['usuario']['id'].' ORDER BY id DESC LIMIT 1;');
          $last = $select_last->fetch(PDO::FETCH_OBJ);
          $count = $select_last->rowCount();

          if($count == 0){ $pass = true; } else {
            $select_resp = $this->mysql->query('SELECT * FROM mensagens WHERE para = '.$last->id.';');
            $resp = $select_resp->rowCount();

            if($resp > 0){ $pass = true; } else { return false; }
          }

          if($pass){
            $cadastra = $this->mysql->prepare('INSERT INTO mensagens (pai, anuncio, mensagem, para, inclusao, publicado) VALUES ('.$_SESSION['usuario']['id'].', :anuncio, :mensagem, 0, :inclusao, 1);');
            $cadastra->bindValue(':anuncio', $dados['anuncio'], PDO::PARAM_STR);
            $cadastra->bindValue(':mensagem', $dados['mensagem'], PDO::PARAM_STR);
            $cadastra->bindValue(':inclusao', date('Y-m-d'), PDO::PARAM_STR);
            $cadastra->execute();
          }

          return $pass;
    }

    public function getAnswer($quest){

          $select_answer = $this->mysql->query('SELECT * FROM mensagens WHERE para = '.$quest.';');
          $answer = $select_answer->fetch(PDO::FETCH_OBJ);
          $answer_count = $select_answer->rowCount();

          if($answer_count == 0){ return false; } else {
            return $answer;
          }
    }

    public function makeAnswer($dados){
      $cadastra = $this->mysql->prepare('INSERT INTO mensagens (pai, anuncio, mensagem, para, inclusao, publicado) VALUES ('.$_SESSION['usuario']['id'].', :anuncio, :mensagem, :para, :inclusao, 1);');
      $cadastra->bindValue(':anuncio', $dados['anuncio'], PDO::PARAM_STR);
      $cadastra->bindValue(':mensagem', $dados['mensagem'], PDO::PARAM_STR);
      $cadastra->bindValue(':para', $dados['para'], PDO::PARAM_STR);
      $cadastra->bindValue(':inclusao', date('Y-m-d'), PDO::PARAM_STR);
      $cadastra->execute();
    }

    public function readQuests($anuncio){
          $select = $this->mysql->prepare('SELECT * FROM mensagens WHERE anuncio = :anuncio AND publicado = 1 AND para = 0;');
          $select->bindValue(':anuncio', $anuncio, PDO::PARAM_STR);
          $select->execute();
          return $select->fetchAll();
    }

    public function readMyQuests($ids){
          $select = $this->mysql->prepare('SELECT * FROM `mensagens` WHERE anuncio in ('.$ids.') AND publicado = 1 AND para = 0;');
          $select->execute();
          return $select->fetchAll();
    }

    public function readMyAnswers($pai){
          $select = $this->mysql->prepare('SELECT * FROM `mensagens` WHERE anuncio in ('.$ids.') AND publicado = 1 AND para = 0;');
          $select->execute();
          return $select->fetchAll();
    }

    public function readMessages($anuncio=null, $id=null, $pai=null){
      if(!empty($anuncio)) {
            $select = $this->mysql->prepare('SELECT * FROM mensagens WHERE anuncio = :anuncio AND publicado = 1 AND para = 0;');
            $select->bindValue(':anuncio', $anuncio, PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll();
      } else if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM mensagens WHERE id = :id AND publicado = 1;');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else if(!empty($pai)) {
            $select = $this->mysql->prepare('SELECT * FROM mensagens WHERE pai = :id AND publicado = 1;');
            $select->bindValue(':id', $pai  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetchAll();
        }else {
            $select = $this->mysql->prepare('SELECT * FROM mensagens WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function readPubAnuncio($nome=null, $id=null){
        if(!empty($nome)) {
            $select = $this->mysql->prepare('SELECT * FROM anuncios WHERE titulo = :nome AND ativ = 1 AND publicado = 1;');
            $select->bindValue(':nome', $nome, PDO::PARAM_STR);
            $select->execute();
            return $select->fetch();
        } else if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM anuncios WHERE id = :id AND ativ = 1 AND publicado = 1;');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else {
            $select = $this->mysql->prepare('SELECT * FROM anuncios WHERE ativ = 1 AND publicado = 1;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function deleteAnuncio($id){
        $delete = $this->mysql->prepare('UPDATE anuncios SET ativ = 0 WHERE id = :id;');
        $delete->bindValue(':id', $id, PDO::PARAM_INT);
        $delete->execute();

        $foto = $this->searchFoto($id);
        foreach($foto as $fotos){ unlink($fotos['local']); }

        $deletef = $this->mysql->prepare('DELETE FROM fotos WHERE id_pai = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}
