<?php

class Cupom{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    //tipo 1 = desconto porcentagem
    //tipo 2 = desconto dinheiro
    //tipo 3 = insere produto no carrinho

    //CRUD

    public function ativaCupom($id){
        $delete = $this->mysql->prepare('UPDATE cupom SET ativo = 1 WHERE id = :id;');
        $delete->bindValue(':id', $id, PDO::PARAM_INT);
        return $delete->execute();
    }

    public function inativaCupom($id){
        $delete = $this->mysql->prepare('UPDATE cupom SET ativo = 0 WHERE id = :id;');
        $delete->bindValue(':id', $id, PDO::PARAM_INT);
        return $delete->execute();
    }

    public function linktCupomToLoja($id_cupom, $id_loja){
      $cadastra = $this->mysql->prepare('INSERT INTO cupom_loja (id_cupom, id_loja, criado) VALUES (:id_cupom, :id_loja, :criado);');
      $cadastra->bindValue(':id_cupom', $id_cupom, PDO::PARAM_INT);
      $cadastra->bindValue(':id_loja', $id_loja, PDO::PARAM_INT);
      $cadastra->bindValue(':criado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
      $cadastra->execute();
    }

    public function insertCupom($dados){

      $dado = $dados['codigo'];
      $consulta = $this->mysql->prepare("SELECT * FROM cupom WHERE codigo=?");
      $consulta->execute([$dado]);
      $registro = $consulta->fetch();
      if ($registro) {
          return 'erro';
      } else {
        $cadastra = $this->mysql->prepare('INSERT INTO cupom (nome, codigo, tipo, valor, criado) VALUES (:nome, :codigo, :tipo, :valor, :criado);');
        $cadastra->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $cadastra->bindValue(':codigo', $dados['codigo'], PDO::PARAM_STR);
        $cadastra->bindValue(':tipo', $dados['tipo'], PDO::PARAM_INT);
        $cadastra->bindValue(':valor', $dados['valor'], PDO::PARAM_INT);
        $cadastra->bindValue(':criado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
        $cadastra->execute();
        return $this->mysql->lastInsertId();
      }
    }

    public function readCupom($id=null, $name=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM cupom WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch(PDO::FETCH_ASSOC);
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM cupom WHERE nome = :nome');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll(PDO::FETCH_ASSOC);
        }else {
            $select = $this->mysql->prepare('SELECT * FROM cupom WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll(PDO::FETCH_ASSOC);
        }

        $select->execute();
        return $select->fetch();
    }

    public function editCupom($dados, $id){
      $consulta = $this->mysql->prepare("SELECT * FROM cupom WHERE codigo=:codigo AND id <> :id");
      $consulta->bindValue(':codigo', $dados['codigo'], PDO::PARAM_STR);
      $consulta->bindValue(':id', $id, PDO::PARAM_INT);
      $consulta->execute();
      $registro = $consulta->fetch();
      if ($registro) {
          return 'erro';
      } else {
        $atualiza = $this->mysql->prepare('UPDATE cupom SET nome=:nome, codigo=:codigo, tipo=:tipo, valor=:valor, atualizado=:atualizado WHERE id = :id ');
        $atualiza->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $atualiza->bindValue(':codigo', $dados['codigo'], PDO::PARAM_STR);
        $atualiza->bindValue(':tipo', $dados['tipo'], PDO::PARAM_INT);
        $atualiza->bindValue(':valor', $dados['valor'], PDO::PARAM_INT);
        $atualiza->bindValue(':atualizado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
        $atualiza->bindValue(':id', $id, PDO::PARAM_INT);
        $atualiza->execute();
      }
    }

    public function deleteCupom($id){
        $deletef = $this->mysql->prepare('DELETE FROM produtos_cat WHERE id = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }

}
