<div class="container zero-form" id="crop-avatar">
    <div class="row">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-user"></span> Novo Anuncio </h3>
            </div>
            <div class="panel-body">

                <!-- <div class="col-xs-12 col-sm-4">

                    <div class="thumbnail avatar-view" onClick="refleshIMG()">
                        <img src="<?php //echo URL_BASE; ?>/assets/images/logo.png" alt="Avatar">
                        <div class="caption">
                        </div>
                    </div>

                </div> -->
                <div class="col-md-12">
                    <div id="aviso"></div>
                    <form action="" method="POST">

                        <div class="input-group">
                            <span class="input-group-addon zero-addon">Título:</span>
                            <input class="form-control" type="text" name="titulo" required="true" aria-required="true">
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon zero-addon">Categoria:</span>
                            <select class="form-control select validation-field" name="categoria" id="areaSelect" required aria-required="true">
                              <option value>Área do serviço</option>
                              <?php foreach($categorias as $categoria): ?>
                                  <option value="<?php echo $categoria['id']; ?>"><?php echo $categoria['nome']; ?></option>
                              <?php endforeach; ?>
                                <!-- <option value="0">Outros</option> -->
                            </select>
                            <span class="input-group-addon zero-addon">Urgente:</span>
                            <select  name="urgente" class="form-control" id="inlineCheckbox1" value="1" data-msg-required="Selecione somente se for urgente.">
                              <option value="0">não</option>
                              <option value="1">sim</option>
                            </select>
                        </div>

                        <?php foreach($categorias as $categoria): ?>
                        <?php $servicos = $serv->readServ(null, null, $categoria['id']); ?>

                        <div class="input-group services hidden" id="servs<?php echo $categoria['id']; ?>">
                            <span class="input-group-addon zero-addon">Serviço de <?php echo $categoria['nome']; ?>:</span>
                            <select class="form-control select services-select validation-field" name="none" id="serv-select<?php echo $categoria['id']; ?>" disabled>
                                <?php foreach($servicos as $servico): ?>
                                    <option value="<?php echo $servico['id']; ?>"><?php echo $servico['nome']; ?></option>
                                <?php endforeach; ?>
                              <option value="0">Outros</option>
                            </select>
                        </div>

                        <?php endforeach; ?>

                        <div class="input-group">
                          <span class="input-group-addon zero-addon">Data para o serviço:</span>
                          <div id="datepicker-container" data-msg-required="Escolha o dia que será feito o serviço.">
                            <input type="text" class="form-control text-center" data-date-end-date="+182d" data-date-start-date="0d" name="diaServico" required="true" aria-required="true" size="10">
                          </div>
                          <span class="input-group-addon zero-addon">Tempo para receber lances:</span>
                          <select class="form-control select" name="tempo">
                              <option value="1">1 mês</option>
                              <option value="2">2 meses</option>
                              <option value="3">3 meses</option>
                              <option value="4">4 meses</option>
                              <option value="5">5 meses</option>
                              <option value="6">6 meses</option>
                          </select>
                          <span class="input-group-addon zero-addon">CEP:</span>
                          <input type="text" maxlength="8" class="form-control validation-field" name="local1cep" required="true" aria-required="true" size="10">
                          <span class="input-group-addon zero-addon">Número:</span>
                          <input type="number" maxlength="20" class="form-control validation-field" name="local1num" required="true" aria-required="true" size="5">
                        </div>
                        <div class="input-group">
                          <span class="input-group-addon zero-addon">Complemento:</span>
                          <input type="text" maxlength="100" class="form-control validation-field" name="local1com" aria-required="true" size="20">
                          <span class="input-group-addon zero-addon">CEP Destino:</span>
                          <input type="text" maxlength="100" class="form-control" name="local2cep" size="10">
                        </div>
                        <div class="input-group">
                          <span class="input-group-addon zero-addon">Número:</span>
                          <input type="number" maxlength="100" class="form-control" name="local2num" size="5">
                          <span class="input-group-addon zero-addon">Complemento:</span>
                          <input type="text" maxlength="100" class="form-control" name="local2com">
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon zero-addon">Descrição:</span>
                            <textarea class="form-control" name="descricao" id="" cols="30" rows="8"></textarea>
                        </div>

                        <hr/>

                        <input class="form-control" type="hidden" name="pai" value="<?php echo $_SESSION['usuario']['id']; ?>">

                        <div class="input-group">
                            <!-- <a class="btn btn-primary" id="enviarCar" href="javascript:void(0)">Enviar</a> -->
                            <input class="btn btn-primary" type="submit" name="" value="Enviar">
                            <a class="btn btn-danger" href="<?php echo URL_BASE; ?>/anuncios/listar">Voltar</a>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

    <!-- <div class="row">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-picture"></span> Fotos carregadas </h3>
            </div>
            <div class="panel-body">
                <div id="uploads"></div>
            </div>
        </div>
    </div> -->

    <!-- Cropping modal -->
    <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form class="avatar-form" action="crop.php" enctype="multipart/form-data" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="avatar-modal-label">Edição de imagem</h4>
                    </div>
                    <div class="modal-body">
                        <div class="avatar-body">

                            <!-- Upload image and data -->
                            <div class="avatar-upload">
                                <input type="hidden" class="avatar-src" name="avatar_src">
                                <input type="hidden" class="avatar-data" name="avatar_data">
                                <label for="avatarInput"></label>
                                <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
                                <!-- passa o id do edit para o crop.php -->
                                <input type="hidden" name="update" value="">
                            </div>

                            <!-- Crop and preview -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="avatar-wrapper"></div>
                                </div>
                            </div>

                            <div class="row avatar-btns">
                                <!--
                                <div class="col-md-9">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary" data-method="rotate" data-option="-90" title="Rotate -90 degrees">Rotate Left</button>
                                        <button type="button" class="btn btn-primary" data-method="rotate" data-option="-15">-15deg</button>
                                        <button type="button" class="btn btn-primary" data-method="rotate" data-option="-30">-30deg</button>
                                        <button type="button" class="btn btn-primary" data-method="rotate" data-option="-45">-45deg</button>
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary" data-method="rotate" data-option="90" title="Rotate 90 degrees">Rotate Right</button>
                                        <button type="button" class="btn btn-primary" data-method="rotate" data-option="15">15deg</button>
                                        <button type="button" class="btn btn-primary" data-method="rotate" data-option="30">30deg</button>
                                        <button type="button" class="btn btn-primary" data-method="rotate" data-option="45">45deg</button>
                                    </div>
                                </div>
                                -->
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-primary btn-block avatar-save" >Subir imagem</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                </form>
            </div>
        </div>
    </div><!-- /.modal -->

    <!-- Loading state -->
    <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>

</div>
