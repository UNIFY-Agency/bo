<div id="overlay">
  <div id="overlay_text">Carregando Pagseguro...</div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
          <h2>Fechar anuncio no lance da empresa <?php echo $dados_comp['nome']; ?>.</h2>
          <!-- porcentagem do site 20% -->
          <h3 style="color: #543594;">
            No valor de <?php echo "R$ ".number_format(($lance['valor'] + ((20 / 100) * $lance['valor'])), 2, ',', '.'); ?>.
          </h3>
        </div>
    </div>
    <hr/>
    <form action="" method="post" name="pagseguroForm" id="pagseguroForm">
    <div class="row">
      <div class="col-xs-10">
        <p>
          Para fechar o anúncio deve pagar apenas 20% do valor, o resto do valor é pago presencialmente para o prestador do serviço.
        </p>
      </div>
      <div class="col-xs-2">
		<div class="radio-group" data-msg-required="Selecione uma dessas opções!">
			<label class="radio-inline">
				<input type="radio" name="metodo" value="pagseguro" data-msg-required="Selecione uma dessas opções!" required> Pagseguro
			</label>
			<label class="radio-inline">
				<input type="radio" name="metodo" value="paypal" data-msg-required="Selecione uma dessas opções!"> Paypal
			</label>
		</div>
        <div class="input-group">
            <input class="btn btn-primary" type="submit" value="Contratar">
            <a class="btn btn-danger" href="<?php echo URL_BASE; ?>/anuncios/ver/<?php echo $publication['id']; ?>">Voltar</a>
        </div>        
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div id="paypal-button-container"></div>
      </div>
    </div>
    <hr/>
    <div class="row">
      <div class="col-xs-6">
            <div class="input-group">
                <span class="input-group-addon zero-addon">Código:</span>
                <input class="form-control" type="text" name="codigo" value="<?php echo $lance['id']; ?>" readonly>

                <span class="input-group-addon zero-addon">Valor:</span>
                <input class="form-control" type="text" name="valor" value="<?php echo "R$ ".number_format(((20 / 100) * $lance['valor']), 2, ',', '.'); ?>" readonly>
            </div>
            <div class="input-group">
                <span class="input-group-addon zero-addon">Descrição:</span>
                <input class="form-control" type="text" name="descricao" value="<?php echo $publication['descricao']; ?>" readonly>
            </div>
            <div class="input-group">
                <span class="input-group-addon zero-addon">Nome:</span>
                <input class="form-control" type="text" name="nome" value="<?php echo $_SESSION['usuario']['nome']; ?>" readonly>
            </div>
            <div class="input-group">
                <span class="input-group-addon zero-addon">E-mail:</span>
                <input class="form-control" type="text" name="email" value="<?php echo $_SESSION['usuario']['email']; ?>" readonly>
            </div>
            <div class="input-group">
                <span class="input-group-addon zero-addon">Telefone:</span>
                <input class="form-control phonemask" type="text" name="telefone" value="" required aria-required="true">
            </div>
            <div class="input-group">
                <span class="input-group-addon zero-addon">Rua:</span>
                <input class="form-control" type="text" name="rua" value="" required aria-required="true">
            </div>
            <div class="input-group">
                <span class="input-group-addon zero-addon">Numero:</span>
                <input class="form-control" type="text" name="numero" value="" required aria-required="true">
            </div>
            <div class="input-group">
                <span class="input-group-addon zero-addon">Complemento:</span>
                <input class="form-control" type="text" name="complemento" value="" required aria-required="true">
            </div>
            <div class="input-group">
                <span class="input-group-addon zero-addon">Bairro:</span>
                <input class="form-control" type="text" name="bairro" value="<?php echo $publication['titulo']; ?>" readonly>
            </div>
            <div class="input-group">
                <span class="input-group-addon zero-addon">Cidade:</span>
                <input class="form-control" type="text" name="cidade" value="<?php echo $publication['titulo']; ?>" readonly>
            </div>
            <div class="input-group">
                <span class="input-group-addon zero-addon">Estado:</span>
                <input class="form-control" type="text" name="estado" value="<?php echo $publication['titulo']; ?>" readonly>
            </div>
            <div class="input-group">
                <span class="input-group-addon zero-addon">CEP:</span>
                <input id="cepspan" class="form-control" type="text" name="cep" value="<?php echo $publication['endereco']; ?>" readonly>
                <input class="form-control" type="hidden" name="quantidade" value="1.0">
            </div>


          <!-- <div class="input-group">
              <span class="input-group-addon zero-addon"><strong>Código: </strong></span>
              <span class="input-group-addon zero-addon"><?php //echo $publication['id']; ?></span>
          </div>
          <div class="input-group">
              <span class="input-group-addon zero-addon"><strong>Valor: </strong></span>
              <span class="input-group-addon zero-addon"><?php //echo "R$ ".number_format(($lance['valor'] + ((20 / 100) * $lance['valor'])), 2, ',', '.'); ?></span>
          </div>
          <div class="input-group">
              <span class="input-group-addon zero-addon"><strong>Título: </strong></span>
              <span class="input-group"><?php //echo $publication['titulo']; ?></span>
          </div> -->


          <!-- <div class="table-responsive">
              <table class="table table-striped table-hover table-bordered table-condensed">
                  <thead>
                  <tr>
                      <th>Código</th>
                      <th>Valor</th>
                      <th>Descrição</th>
                      <th>Título</th>
                      <th>E-mail</th>
                      <th>Telefone</th>
                      <th>Rua</th>
                      <th>Número</th>
                      <th>Bairro</th>
                      <th>Cidade</th>
                      <th>Estado</th>
                      <th>Cep</th>
                  </tr>
                  </thead>
                  <tbody>
                    <tr>
                        <td></td>
                    </tr>
                  </tbody>
              </table>
          </div> -->
      </div>
      <div class="col-xs-6">
        <div class="thumbnail avatar-view" id="map" style="height: 400px; width: 100%;">
        </div>
      </div>
    </div>
    </form>
</div>
<script src="https://www.paypal.com/sdk/js?client-id=AdMqAqHmIVgFyPaTDakI4myPCBc4REN7LJsOdZxIbjrh2QE4hP50pl3tIVbkDUAdBsHf7X4Xz58x9aX0&currency=BRL"></script>
