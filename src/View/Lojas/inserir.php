<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3><?php echo $module; ?> <small>Cadastre uma nova <?php echo $module; ?>.</small></h3>
      </div>

      <?php include "src/View/Includes/search.php"; ?>

    </div>

    <div class="clearfix"></div>

    <?php if($mensagem_erro): ?>

      <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <?php echo $mensagem_erro; ?>
      </div>

    <?php endif; ?>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-home"></i> Formulário: Cadastro de <?php echo $module; ?></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Configurações 1</a>
                  </li>
                  <li><a href="#">Configurações 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form id="create_store_form" data-parsley-validate class="form-horizontal form-label-left" method="post" action="">

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nome da Loja <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="first-name" required="required" name="nome" class="form-control col-md-7 col-xs-12" title="Nome da Marca">
                  <span class="fa fa-question form-control-feedback right" aria-hidden="true"></span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Segmento <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="email" name="segmento" required="required" class="form-control col-md-7 col-xs-12" title="Segmento da Marca EX: Sushi, Pizza, Hamburguer...">
                  <span class="fa fa-question form-control-feedback right" aria-hidden="true"></span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Código <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="codigo" required="required" class="form-control col-md-7 col-xs-12" title="Código da Marca é a inicial do Nome EX: Sushi Rão = S" maxlength="1">
                  <span class="fa fa-question form-control-feedback right" aria-hidden="true"></span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Marca</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div id="gender" class="btn-group" data-toggle="buttons">

                    <?php foreach($marcas_combine as $marca): ?>
                      <label class="btn btn-default marca-paises" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default" >
                        <span class="hidden">
                          <?php
                            foreach ($marca as $pais) {
                              if($pais['ativo']){
                                $paises_str[] = $pais['pais'];
                              }
                            }
                            $paises_hide = array_diff($paises, $paises_str);
                            echo implode(',', $paises_hide);
                            unset($paises_str);
                            ?>
                          </span>
                        <input class="" type="radio" name="marca" value="<?php echo $marca[0]['id']; ?>"> &nbsp; <?php echo $marca[0]['nome']; ?> &nbsp;
                      </label>
                    <?php endforeach; ?>

                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Paises</label>
                <div class="col-md-9 col-sm-9 col-xs-12" id="paises_list">

                  <?php foreach($paises as $pais): ?>
                      <div class="pais_input"><label>
                        <input type="checkbox" name="<?php echo $pais['codigo']; ?>" /> <?php echo $pais['codigo']; ?>
                      </label></div>
                  <?php endforeach; ?>

              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Descrição</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <textarea id="message" class="form-control" name="descricao" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10"></textarea>
                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <a href="<?php echo URL_BASE; ?>/marcas/listar" class="btn btn-primary" type="button">Voltar</a>
		              <a href="" class="btn btn-primary" type="reset">Limpar</a>
                  <input class="btn btn-success" type="submit" name="submit" value="Enviar">
                  <!-- <a href="#" type="submit" class="btn btn-success" onClick="document.getElementById('create_client_form').submit();">Enviar</a> -->
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
