<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3><?php echo $module; ?> <small>veja a listagem de <?php echo strtolower($module); ?>.</small></h3>
      </div>

      <?php include "src/View/Includes/search.php"; ?>

    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-star"></i> Listagem de <?php echo $module; ?></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Configurações 1</a>
                  </li>
                  <li><a href="#">Configurações 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <p class="text-muted font-13 m-b-30">
              Use o marcador para determinar quantos registros quer ver por página e o campo de busca para filtrar qualquer dado.
            </p>
            <table id="datatable" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nome</th>
                  <th>Segmento</th>
                  <th>Código</th>
                  <th>Criado</th>
                  <th>Ações</th>
                </tr>
              </thead>


              <tbody>
                <?php foreach($marcas_combine as $marca): ?>
                    <tr>
                        <td><?php echo $marca[0]['id']; ?></td>
                        <td><?php echo $marca[0]['nome']; ?></td>
                        <td><?php echo $marca[0]['segmento']; ?></td>
                        <td><?php echo $marca[0]['codigo']; ?></td>
                        <td><?php echo date('Y/m/d', strtotime($marca[0]['criado'])); ?></td>
                        <td>
                          <div class="btn-group">
                            <a class="btn btn-default" href="<?php echo URL_BASE; ?>/marcas/ver/<?php echo $marca[0]['id']; ?>" title="Ver cliente"><i class="fa fa-eye"></i></a>
                            <a class="btn btn-default" href="<?php echo URL_BASE; ?>/marcas/editar/<?php echo $marca[0]['id']; ?>" title="Editar cliente"><i class="fa fa-edit"></i></a>
                          </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- /page content -->

<!-- Small modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
        </button>
        <h4 class="modal-title" id="myModalLabel2">Excluir Cliente <span class="del-client-id"></span></h4>
      </div>
      <div class="modal-body">
        <h4 class="del-client-name"></h4>
        <p>ATENÇÃO uma vez que há muitos dados interligados com os clientes cadastrados o usuário não é deletado da base de dados e sim é posto como inativo, dessa forma não pode mas fazer login e/ou fazer compras na plataforma Rão Online.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal" style="margin-top: 19px;">Cancelar</button>
        <!-- <a href="" type="button" class="btn btn-primary del-client-link">Excluir</a> -->
        <form id="del-client-form" action="<?php echo URL_BASE; ?>/clientes/listar/del" method="post">
          <input type="hidden" name="del-client-id"><br>
          <input type="submit" class="btn btn-primary" value="Excluir">
        </form>
      </div>

    </div>
  </div>
</div>
<!-- /modals -->
