<!-- footer content -->
        <footer>
          <div class="pull-right">
            Omnie Admin designed by <a href="https://unify.pt" target="_blank">Unify</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/nprogress/nprogress.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/moment/min/moment.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-datetimepicker -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <!-- Ion.RangeSlider -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
    <!-- Bootstrap Colorpicker -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <!-- jquery.inputmask -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- jQuery Knob -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/gauge.js/dist/gauge.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/pdfmake/build/vfs_fonts.js"></script>
    <!-- Skycons -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/skycons/skycons.js"></script>
    <!-- Flot
    <script src="<?php echo URL_BASE; ?>/assets/vendor/Flot/jquery.flot.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins
    <script src="<?php echo URL_BASE; ?>/assets/vendor/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="<?php echo URL_BASE; ?>/assets/vendor/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/select2/dist/js/select2.full.min.js"></script>
    <!-- Autosize -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/starrr/dist/starrr.js"></script>
    <!-- Cropper -->
    <script src="<?php echo URL_BASE; ?>/assets/vendor/cropper/dist/cropper.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo URL_BASE; ?>/js/custom.js"></script>
    <script src="<?php echo URL_BASE; ?>/js/main_custom.js"></script>
