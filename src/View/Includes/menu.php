<?php

$loader->get('src/Model/Marcas');
$marcas = new Marcas(new Config());
$marcas_menu = $marcas->getMarcasName();

?>

<div class="col-md-3 left_col menu_fixed">
  <div class="left_col scroll-view">
    <div class="navbar nav_title hidden" style="border: 0;">
      <a href="./" class="site_title"><span>Omnie Admin</span></a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_pic">
        <img src="<?php echo URL_BASE; ?>/assets/images/img.jpg" alt="..." class="img-circle profile_img">
      </div>
      <div class="profile_info">
        <span>Bem vindo ao,</span>
        <h2>Omnie Admin</h2>
      </div>
    </div>
    <!-- /menu profile quick info -->

    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <h3>Painel</h3>
        <ul class="nav side-menu">
          <li><a href="<?php echo URL_BASE; ?>/inicio"><i class="fa fa-home"></i> Dashboard</a></li>
          <li><a><i class="fa fa-sitemap"></i> Marcas <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="<?php echo URL_BASE; ?>/marcas/listar">Listar Marcas</a></li>
              <li><a href="<?php echo URL_BASE; ?>/marcas/inserir">Criar nova Marca</a></li>

              <?php foreach($marcas_menu as $marca_menu): ?>
                <li><a href="<?php echo URL_BASE.'/marcas/ver/'.$marca_menu['id']; ?>"><?php echo $marca_menu['nome']; ?></a></li>
              <?php endforeach; ?>
            </ul>
          </li>
          <li><a><i class="fa fa-cubes"></i> Produtos <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="<?php echo URL_BASE; ?>/produtos/listar">Listar Produtos</a></li>
              <?php if($_SESSION['admin']['nivel'] > 0): ?>
                <li><a href="<?php echo URL_BASE; ?>/produtos/inserir">Novo Produto</a></li>
                <li class="active"><a>Categorias<span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: block;">
                    <li><a href="<?php echo URL_BASE; ?>/categorias/listar">Listar</a></li>
                    <li><a href="<?php echo URL_BASE; ?>/categorias/inserir">Criar</a></li>
                  </ul>
                </li>
              <?php endif; ?>
            </ul>
          </li>
          <?php if($_SESSION['admin']['nivel'] > 0): ?>
          <li><a><i class="fa fa-users"></i> Usuários <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="<?php echo URL_BASE; ?>/clientes/listar">Listar Usuários</a></li>
                <li><a href="<?php echo URL_BASE; ?>/clientes/inserir">Cadastrar novo Usuário</a></li>
              <li><a href="<?php echo URL_BASE; ?>/clientes/pesquisa">Pesquisar Usuário</a></li>
            </ul>
          </li>
          <?php endif; ?>
        </ul>
      </div>
      <?php if($_SESSION['admin']['nivel'] > 0): ?>
      <div class="menu_section hidden">
        <h3>Ferramentas</h3>
        <ul class="nav side-menu">
          <li><a href="./mensagens"><i class="fa fa-envelope"></i> Mensagens </a>
          </li>
          <li><a><i class="fa fa-bug"></i> Erros <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="./tickets">Central de Tickets</a></li>
              <li><a href="#">Relatar um problema</a></li>
            </ul>
          </li>
          <li class="hidden"><a href="javascript:void(0)"><i class="fa fa-phone"></i> Chat Online <span class="label label-success pull-right">Em breve</span></a></li>
        </ul>
      </div>
      <?php endif; ?>

    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
      <a data-toggle="tooltip" data-placement="top" title="Settings">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="FullScreen" onclick="openFullscreen();">
        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Lock">
        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Logout" href="./logout.php">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
      </a>
    </div>
    <!-- /menu footer buttons -->
  </div>
</div>

<!-- top navigation -->
<div class="top_nav">
  <div class="nav_menu">
    <nav>
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>

      <ul class="nav navbar-nav navbar-right">
        <li class="">
          <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <img src="<?php echo URL_BASE; ?>/assets/images/img.jpg" alt=""><?php echo $_SESSION['admin']['nome'] ?>
            <span class=" fa fa-angle-down"></span>
          </a>
          <ul class="dropdown-menu dropdown-usermenu pull-right">
            <li><a href="<?php echo URL_BASE; ?>/logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>
</div>
<!-- /top navigation -->
