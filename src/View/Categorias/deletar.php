
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-xs-12">
            <form action="" method="post">

                <h2>Remover a categoria <?php echo $categoria['nome']; ?></h2>

                <hr/>

                <div class="input-group">
                    <input class="btn btn-primary" type="submit" value="Sim">
                    <a class="btn btn-danger" href="<?php echo URL_BASE; ?>/categorias/listar">Não</a>
                </div>

            </form>

        </div>
    </div>
</div>
