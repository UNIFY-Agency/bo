<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3><?php echo $module; ?> <small>veja a listagem de <?php echo strtolower($module); ?>.</small></h3>
      </div>

      <?php include "src/View/Includes/search.php"; ?>

    </div>

    <div class="clearfix"></div>

    <div class="alert alert-danger alert-dismissible notifications <?php echo empty($mensagem_erro) ? 'hidden' : '' ; ?>" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
      </button>
      <i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> <?php echo $mensagem_erro; ?>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-shopping-cart"></i> Listagem de <?php echo $module.' '.$action; ?> <a class="btn btn-primary reflesh-btn" href="javascript:void(0)" title="Atualizar lista"><i class="fa fa-refresh"></i></a></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Configurações 1</a>
                  </li>
                  <li><a href="#">Configurações 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <p class="text-muted font-13 m-b-30">
              Use o marcador para determinar quantos registros quer ver por página e o campo de busca para filtrar qualquer dado.
            </p>
            <table id="datatable" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Referência</th>
                  <th>Entrega</th>
                  <th>Valor</th>
                  <th>Dia</th>
                  <th>Horário</th>
                  <th>Cliente</th>
                  <th>Código Postal</th>
                  <th>Loja</th>
                  <th>Ações</th>
                </tr>
              </thead>


              <tbody>
                <?php foreach($pedidos_lista as $pedido): ?>
                    <tr class="<?php if($pedido['entrega'] == 2){ echo 'info'; } else if($pedido['entrega'] == 3 || $pedido['entrega'] == 4){ echo 'danger'; } ?>">
                        <td><?php echo $pedido['id']; ?></td>
                        <td><?php echo $pedido['referencia']; ?></td>
                        <td><?php if($pedido['entrega'] == 1){ echo 'Delivery'; } else if($pedido['entrega'] == 2){ echo 'Take Away'; } else if($pedido['entrega'] == 3){ echo $pedido['adiantado'].' <i class="fa fa-clock-o"></i> Adiantamento Delivery'; } else if($pedido['entrega'] == 4){ echo $pedido['adiantado'].' <i class="fa fa-clock-o"></i> Adiantamento Take Away'; } ?></td>
                        <td><?php echo $pedido['valor']; ?>€</td>
                        <td><?php echo date('Y/m/d', strtotime($pedido['criado'])); ?></td>
                        <td><?php echo date('H:i', strtotime($pedido['criado'])); ?></td>
                        <td><?php echo $pedido['nome']; ?></td>
                        <td><?php echo $pedido['codigo_postal']; ?></td>
                        <td><?php echo $pedido['id_loja']; ?></td>
                        <td>
                          <div class="btn-group">
                            <a class="btn btn-default view-btn" href="#" title="Ver Pedido" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-eye"></i></a>
                            <a href="javascript:void(0);" title="Avançar encomenda" class="btn btn-primary advance-order <?php echo is_null($options['next']) ? 'hidden' : '' ; ?>" next="<?php echo $options['next']; ?>" action="<?php echo $options['next_page']; ?>"><i class="fa fa-mail-forward"></i></a>
                          </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- /page content -->

<!-- Small modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
        </button>
        <h4 class="modal-title" id="myModalLabel2">Cancelar pedido  <span class="del-client-id"></span></h4>
      </div>
      <div class="modal-body">
        <h4 class="del-client-name"></h4>
        <p>ATENÇÃO ao cancelar o pedido ele jamais poderá voltar ao estado anterior então tenha certeza de que realmente quer cancelar esse pedido. Por favor, preencha o motivo do cancelamento abaixo.</p>
        <div class="x_content">
          <form id="cancel_order_form" data-parsley-validate class="form-horizontal form-label-left" method="post" action="">
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <input name="del-client-id" type="hidden" value="" />
                <textarea id="message" required="required" class="form-control" rows="5" name="message" data-parsley-trigger="keyup" data-parsley-minlength="5" data-parsley-maxlength="140" data-parsley-minlength-message="Você precisa preencher o motivo para cancelar o pedido"
                  data-parsley-validation-threshold="10"></textarea>
              </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <input type="button" class="btn btn-default pull-left" data-dismiss="modal" value="Fechar" />
                <input class="btn btn-danger pull-right" type="submit" name="submit" value="Cancelar Pedido">
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<!-- /modals -->

<!-- Large modal -->
<?php include 'detalhes_modal.php'; ?>
<!-- /modals -->
