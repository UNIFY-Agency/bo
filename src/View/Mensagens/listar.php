<div class="container">
    <div class="row">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-user"></span> Mensagens </h3>
            </div>
        </div>

        <div class="col-xs-12">
          <div class="table-responsive">
              <table class="table table-striped table-hover table-bordered table-condensed">
                  <thead>
                  <tr>
                      <th>Usuário</th>
                      <th>Pergunta</th>
                      <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php if($messages): ?>
                  <?php foreach($messages as $message): ?>
                  <?php if($message['publicado'] == 1){ $ativ_val = array('btn-success', 'glyphicon-ok', 0); }else{ $ativ_val = array('btn-warning', 'glyphicon-remove', 1); } ?>
                  <?php $pai = $users->readUsuario(null, $message['pai']); ?>
                  <?php $answer = $mensagens->getAnswer($message['id']); ?>
                      <tr>
                          <td><?php echo $message['mensagem']; ?></td>
                          <td><a href="../../empresa/ver/<?php echo $message['pai']; ?>"><?php echo $pai['nome']; ?></a></td>
                          <td><a class="btn btn-primary" href="../anuncios/ver/<?php echo $message['anuncio']; ?>">Ver</a>
                  <?php if($answer): ?>
                          </td>
                      </tr>
                      <tr>
                          <td><?php echo $answer->mensagem; ?></td>
                          <td style="color: green;">Resposta</td>
                          <td></td>
                      </tr>
                  <?php elseif($_SESSION['usuario']['tipo'] != 2): ?>
                            <a class="btn btn-primary" href="javascript:promptUrl(<?php echo $message['anuncio'].','.$message['id']; ?>);">Responder</a>
                          </td>
                      </tr>
                  <?php endif; ?>
                  <?php endforeach; ?>
                <?php endif; ?>
                  </tbody>
              </table>
          </div>
        </div>
    </div>
</div>
