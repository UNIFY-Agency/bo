<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Ficha de Usuário <small>Informações sobre Usuário</small></h3>
      </div>

      <?php include "src/View/Includes/search.php"; ?>

    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-user"></i> Usuário #<?php echo $cliente['id']; ?></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Configurações 1</a>
                  </li>
                  <li><a href="#">Configurações 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <section class="content invoice">
              <!-- title row -->
              <div class="row">
                <div class="col-xs-12 invoice-header">
                  <h1>
                      <i class="fa fa-globe"></i> <?php echo $cliente['nome']; ?>.
                      <small class="pull-right">Cadastrado: <?php echo date('d/m/Y', strtotime($cliente['criado'])); ?></small>
                  </h1>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  <strong>Contatos</strong>
                  <address>
                      <br>E-mail: <?php echo $cliente['email']; ?>
                      <br>Telefone: <?php echo $cliente['tel']; ?>
                      <br>Telemóvel: <?php echo $cliente['tel2']; ?>
                      <br>Data de Nascimento: <?php echo $cliente['aniversario']; ?>
                  </address>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              
              <!-- /.row -->

              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-xs-12">
                  <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                  <button class="btn btn-success pull-right hidden"><i class="fa fa-credit-card"></i> Submit Payment</button>
                  <button class="btn btn-primary pull-right hidden" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
                  <?php if(empty($_SESSION['admin']['franqueado'])): ?>
                    <a href="#" data-toggle="modal" data-target=".bs-example-modal-sm" title="Excluir cliente" class="btn btn-danger pull-right" style="margin-right: 5px;">Excluir</a>
                  <?php endif; ?>
                  <a href="<?php echo URL_BASE; ?>/clientes/listar" class="btn btn-primary pull-right" style="margin-right: 5px;">Voltar</a>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->

<!-- Small modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
        </button>
        <h4 class="modal-title" id="myModalLabel2">Excluir Cliente <?php echo '#'.$cliente['id']; ?></h4>
      </div>
      <div class="modal-body">
        <h4><?php echo $cliente['nome']; ?></h4>
        <p>ATENÇÃO uma vez que há muitos dados interligados com os clientes cadastrados o usuário não é deletado da base de dados e sim é posto como inativo, dessa forma não pode mas fazer login e/ou fazer compras na plataforma Rão Online.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal" style="margin-top: 19px;">Cancelar</button>
        <!-- <a href="" type="button" class="btn btn-primary del-client-link">Excluir</a> -->
        <form id="del-client-form" action="<?php echo URL_BASE; ?>/clientes/ver/del" method="post">
          <input type="hidden" name="del-client-id" value="<?php echo $cliente['id']; ?>"><br>
          <input type="submit" class="btn btn-primary" value="Excluir">
        </form>
      </div>

    </div>
  </div>
</div>
<!-- /modals -->

<!-- Large modal -->
<?php $loader->get('src/View/Pedidos/detalhes_modal'); ?>
<!-- /modals -->
