<?php

if(!isset($module) or !is_string($module)){
    header("HTTP/1.0 500 Internal Server Error");
    echo '<h1>Variável $include não encontrada!</h1>';
    exit;
}

include 'Includes/head.php';
include 'Includes/menu.php';

if(file_exists('src'.DS.'View'.DS.$module.DS.$action.'.php')){ include $module.DS.$action . '.php'; }
else if($module == 'Inicio') { include 'Includes/index.php'; }
else { include 'Includes/404.php'; }

include 'Includes/footer.php';

echo "<script>var URL_BASE = '".URL_BASE."';</script>";
echo "<script>var USER_SID = '".$_SESSION['admin']['id']."';</script>";
//echo "<script>var LOJA_ID = '".$_SESSION['admin']['franqueado']['id']."';</script>";

//incluindo JS do modulo
$filename = 'js/Controller/'.$module.'.js';

if (file_exists($filename)) {
    echo '<script src="'.URL_BASE.'/'.$filename.'"></script>';
} else {
    //echo $filename;
}

echo "<script>var URL_BASE = '".URL_BASE."';</script>";

echo '</body></html>';
?>
