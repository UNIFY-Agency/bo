<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3><?php echo $module; ?> <small>veja a listagem de <?php echo strtolower($module); ?>.</small></h3>
      </div>

      <?php include "src/View/Includes/search.php"; ?>

    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-table"></i> Listagem de <?php echo $module; ?></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Configurações 1</a>
                  </li>
                  <li><a href="#">Configurações 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <p class="text-muted font-13 m-b-30">
              Use o marcador para determinar quantos registros quer ver por página e o campo de busca para filtrar qualquer dado.
            </p>
            <table id="datatable" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nome</th>
                  <th>Categoria</th>
                  <th>SKU</th>
                  <th>Preço</th>
                  <th>Ações</th>
                </tr>
              </thead>


              <tbody>
                <?php foreach($produtos as $produto): ?>
                    <tr>
                        <td><?php echo $produto['id']; ?></td>
                        <td><?php echo $produto['nome']; ?></td>
                        <td><?php echo $produto['cat']; ?></td>
                        <td><?php echo $produto['sku']; ?></td>
                        <td><?php echo '€' . number_format($produto['preco'], 2); ?></td>
                        <td>
                          <div class="btn-group">
                            <?php if(empty($_SESSION['admin']['franqueado'])): ?>
                              <a class="btn btn-default" href="<?php echo URL_BASE; ?>/produtos/ver/<?php echo $produto['id']; ?>" title="Ver produto"><i class="fa fa-eye"></i></a>
                              <a class="btn btn-default" href="<?php echo URL_BASE; ?>/produtos/editar/<?php echo $produto['id']; ?>" title="Editar produto"><i class="fa fa-edit"></i></a>
                              <?php if($produto['ativo']): ?>
                                <a class="btn btn-default inat-btn" href="javascript:void(0);" title="Inativar produto"><i class="fa fa-close"></i></a>
                              <?php else: ?>
                                <a class="btn btn-default ativ-btn" href="javascript:void(0);" title="Ativar produto"><i class="fa fa-check"></i></a>
                            <?php endif; endif; ?>
                          </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- /page content -->

<!-- Small modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
        </button>
        <h4 class="modal-title" id="myModalLabel2">Inativar Produto <span class="del-client-id"></span></h4>
      </div>
      <div class="modal-body">
        <h4 class="del-client-name"></h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal" style="margin-top: 19px;">Cancelar</button>
        <!-- <a href="" type="button" class="btn btn-primary del-client-link">Excluir</a> -->
        <form id="del-client-form" action="<?php echo URL_BASE; ?>/clientes/listar/del" method="post">
          <input type="hidden" name="del-client-id"><br>
          <input type="submit" class="btn btn-primary" value="Inativar">
        </form>
      </div>

    </div>
  </div>
</div>
<!-- /modals -->
