<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3><?php echo $module; ?> <small>Edite <?php echo $module; ?>.</small></h3>
      </div>

      <?php include "src/View/Includes/search.php"; ?>

    </div>

    <div class="clearfix"></div>

    <?php if($mensagem_erro): ?>

      <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <?php echo $mensagem_erro; ?>
      </div>

    <?php endif; ?>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-cutlery"></i> Formulário: Editar produto <?php echo $produto['nome']; ?></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Configurações 1</a>
                  </li>
                  <li><a href="#">Configurações 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form id="edit_product_form" data-parsley-validate class="form-horizontal form-label-left" method="post" action="" enctype="multipart/form-data">
              <div class="form-group">
                  <div class="image view view-first">
                    <img style="width: 10%; display: block; margin: 0 auto;" src="<?php echo $produto['image_url']; ?>" alt="image">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Trocar imagem</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <label class="btn btn-default btn-file">
                    Procurar <input type="file" name="foto" style="display: none;">
                  </label>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Título do Produto <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="first-name" required="required" name="nome" class="form-control col-md-7 col-xs-12" value="<?php echo $produto['nome']; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Preço <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" pattern="^\d*(\.\d{0,2})?$" required="required" name="preco" class="form-control col-md-7 col-xs-12" placeholder="Utilize ponto para números quebrados ex: 12.50" value="<?php echo $produto['preco']; ?>">
                  <span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Categoria <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select name="categoria" class="form-control" required>
                    <?php foreach ($categorias as $key => $value): ?>
                      <option value="<?php echo $value['id']; ?>" <?php if($value['id'] == $produto['categoria']){ echo 'selected'; } ?>><?php echo $value['nome']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Descrição <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <textarea id="message" class="form-control" name="descricao" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10">
                    <?php echo $produto['descricao']; ?>
                  </textarea>
                </div>
              </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <a href="<?php echo URL_BASE; ?>/produtos/listar" class="btn btn-primary" type="button">Voltar</a>
		              <a href="" class="btn btn-primary" type="reset">Limpar</a>
                  <input class="btn btn-success" type="submit" name="submit" value="Enviar">
                  <!-- <a href="#" type="submit" class="btn btn-success" onClick="document.getElementById('create_client_form').submit();">Enviar</a> -->
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
