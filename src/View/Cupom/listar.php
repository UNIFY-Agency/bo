<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3><?php echo $module; ?> <small>veja a listagem de <?php echo strtolower($module); ?>.</small></h3>
      </div>

      <?php include "src/View/Includes/search.php"; ?>

    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-map-marker"></i> Listagem de <?php echo $module; ?></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Configurações 1</a>
                  </li>
                  <li><a href="#">Configurações 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <p class="text-muted font-13 m-b-30">
              Use o marcador para determinar quantos registros quer ver por página e o campo de busca para filtrar qualquer dado.
            </p>
            <table id="datatable" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nome</th>
                  <th>Código</th>
                  <th>Tipo</th>
                  <th>Valor</th>
                  <th>Ações</th>
                </tr>
              </thead>


              <tbody>
                <?php foreach($cupom_lista as $cupom_item): ?>
                    <tr>
                        <td><?php echo $cupom_item['id']; ?></td>
                        <td><?php echo $cupom_item['nome']; ?></td>
                        <td><?php echo $cupom_item['codigo']; ?></td>
                        <td><?php if($cupom_item['tipo'] == 1){ echo 'Porcentagem'; } else if($cupom_item['tipo'] == 2){ echo 'Valor Direto'; } else { echo 'Id Produto'; } ?></td>
                        <td><?php echo $cupom_item['valor']; ?><?php if($cupom_item['tipo'] == 1){ echo '%'; } else if($cupom_item['tipo'] == 2){ echo '€'; }?></td>
                        <td>
                          <div class="btn-group">
                            <?php if($cupom_item['ativo']): ?>
                              <a class="btn btn-danger inat-btn" href="javascript:void(0);" title="Inativar produto"><i class="fa fa-close"></i></a>
                            <?php else: ?>
                              <a class="btn btn-success ativ-btn" href="javascript:void(0);" title="Ativar produto"><i class="fa fa-check"></i></a>
                            <?php endif; ?>
                          </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- /page content -->
