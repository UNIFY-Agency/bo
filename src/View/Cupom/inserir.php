<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3><?php echo $module; ?> <small>Cadastre um novo <?php echo $module; ?>.</small></h3>
      </div>

      <?php include "src/View/Includes/search.php"; ?>

    </div>

    <div class="clearfix"></div>

    <?php if($mensagem_erro): ?>

      <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <?php echo $mensagem_erro; ?>
      </div>

    <?php endif; ?>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-tag"></i> Formulário: Cadastro de <?php echo $module; ?></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Configurações 1</a>
                  </li>
                  <li><a href="#">Configurações 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form id="create_cupom_form" data-parsley-validate class="form-horizontal form-label-left" method="post" action="">

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nome <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="first-name" name="nome" class="form-control col-md-7 col-xs-12" required="required">
                  <span class="fa fa-road form-control-feedback right" aria-hidden="true"></span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Código <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="codigo" required="required" class="form-control">
                  <span class="fa fa-home form-control-feedback right" aria-hidden="true"></span>
                </div>
              </div>
              <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Tipo <span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <select id="heard" name="tipo" class="form-control" required>
                        <option value="1">Porcentagem</option>
                        <option value="2">Valor</option>
                        <option value="3">Id de Produto</option>
                      </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Valor <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="valor" class="form-control"  required="required">
                  <span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Loja</label>
                <div class="col-md-9 col-sm-9 col-xs-12">

                    <?php foreach($lojas_lista as $loja_item): ?>
                        <div><label>
                          <input type="checkbox" class="js-switch" name="<?php echo $loja_item['id']; ?>" /> <?php echo $loja_item['nome']; ?>
                        </label></div>
                    <?php endforeach; ?>

                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <a href="<?php echo URL_BASE; ?>/cupom/listar" class="btn btn-primary" type="button">Voltar</a>
		              <a href="javascript:void(0);" class="btn btn-primary" type="reset">Limpar</a>
                  <input class="btn btn-success" type="submit" name="submit" value="Enviar">
                  <!-- <a href="#" type="submit" class="btn btn-success" onClick="document.getElementById('create_client_form').submit();">Enviar</a> -->
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
