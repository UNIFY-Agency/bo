<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3><?php echo $module; ?> <small>veja a listagem de <?php echo strtolower($module); ?>.</small></h3>
      </div>

      <?php include "src/View/Includes/search.php"; ?>

    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-map-marker"></i> Listagem de <?php echo $module; ?></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Configurações 1</a>
                  </li>
                  <li><a href="#">Configurações 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <p class="text-muted font-13 m-b-30">
              Use o marcador para determinar quantos registros quer ver por página e o campo de busca para filtrar qualquer dado.
            </p>
            <table id="datatable" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Rua</th>
                  <th>Código Postal</th>
                  <th>Loja</th>
                  <th>Taxa de Entrega</th>
                  <th>Ações</th>
                </tr>
              </thead>


              <tbody>
                <?php foreach($enderecos_lista as $endereco): ?>
                    <tr>
                        <td><?php echo $endereco['id']; ?></td>
                        <td><?php echo $endereco['nome_rua']; ?></td>
                        <td><?php echo $endereco['codigo_postal']; ?></td>
                        <td><?php echo $lojas_lista[$endereco['id_loja']]['nome']; ?></td>
                        <td><?php if(empty($endereco['taxa_entrega'])){ echo $lojas_lista[$endereco['id_loja']]['taxa_entrega']; } else { echo $endereco['taxa_entrega']; } ?></td>
                        <td>
                          <div class="btn-group">
                            <a class="btn btn-default" href="<?php echo URL_BASE; ?>/enderecos/editar/<?php echo $endereco['id']; ?>" title="Editar Endereço"><i class="fa fa-edit"></i></a>
                          </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- /page content -->
