<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3><?php echo $module; ?> <small>Edição de <?php echo $module; ?>.</small></h3>
      </div>

      <?php include "src/View/Includes/search.php"; ?>

    </div>

    <div class="clearfix"></div>

    <?php if($mensagem_erro): ?>

      <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <?php echo $mensagem_erro; ?>
      </div>

    <?php endif; ?>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-user"></i> Formulário: Editar endereço <?php echo $endereco['codigo_postal']; ?></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Configurações 1</a>
                  </li>
                  <li><a href="#">Configurações 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form id="edit_endereco_form" data-parsley-validate class="form-horizontal form-label-left" method="post" action="">

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nome da Rua
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="first-name" name="nome_rua" class="form-control col-md-7 col-xs-12" value="<?php echo $endereco['nome_rua']; ?>">
                  <span class="fa fa-road form-control-feedback right" aria-hidden="true"></span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Localidade
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="localidade" class="form-control col-md-7 col-xs-12" value="<?php echo $endereco['localidade']; ?>">
                  <span class="fa fa-map form-control-feedback right" aria-hidden="true"></span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Código Postal <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="codigo_postal" required="required" class="form-control" data-inputmask="'mask' : '9999-999'" value="<?php echo $endereco['codigo_postal']; ?>">
                  <span class="fa fa-map-marker form-control-feedback right" aria-hidden="true"></span>
                </div>
              </div>
              <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Loja</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <select id="heard" name="id_loja" class="form-control" required>

                        <?php foreach($lojas_lista as $loja): ?>
                            <option value="<?php echo $loja['id']; ?>" <?php if($loja['id'] == $endereco['id_loja']){ echo 'selected'; } ?>><?php echo $loja['nome']; ?></option>
                        <?php endforeach; ?>
                      </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Taxa específica</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="taxa_entrega" class="form-control" value="<?php echo $endereco['taxa_entrega']; ?>">
                  <span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Ativo</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <div><label>
                    <input type="checkbox" class="js-switch" name="ativo" <?php if($endereco['ativo'] == 1){ echo 'checked'; } ?> />
                  </label></div>
                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <a href="<?php echo URL_BASE; ?>/enderecos/pesquisa" class="btn btn-primary" type="button">Voltar</a>
		              <a href="" class="btn btn-primary" type="reset">Limpar</a>
                  <input class="btn btn-success" type="submit" name="submit" value="Enviar">
                  <!-- <a href="#" type="submit" class="btn btn-success" onClick="document.getElementById('create_client_form').submit();">Enviar</a> -->
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
