$(document).on('keydown', 'input[pattern]', function(e) {
  var input = $(this);
  var oldVal = input.val();
  var regex = new RegExp(input.attr('pattern'), 'g');

  setTimeout(function() {
    var newVal = input.val();
    if (!regex.test(newVal)) {
      input.val(oldVal);
    }
  }, 0);
});

$(document).on('click', '.inat-btn', function(){

  var btn = $(this);
  var row = $(this).parent().parent().parent();
  var id = row.find("td:nth-child(1)").html();

  $.post( URL_BASE + '/produtos/inativar', {id}, function( result ) {
      console.log(result);
      $(btn).find('i').removeClass();
      $(btn).find('i').addClass('fa fa-check');
      $(btn).removeClass();
      $(btn).addClass('btn btn-default ativ-btn');
      //window.location.replace(URL_BASE + '/produtos/listar');
  });

});

$(document).on('click', '.ativ-btn', function(){

  var btn = $(this);
  var row = $(this).parent().parent().parent();
  var id = row.find("td:nth-child(1)").html();

  $.post( URL_BASE + '/produtos/ativar', {id}, function( result ) {
      console.log(result);
      $(btn).find('i').removeClass();
      $(btn).find('i').addClass('fa fa-close');
      $(btn).removeClass();
      $(btn).addClass('btn btn-default inat-btn');
      //window.location.replace(URL_BASE + '/produtos/listar');
  });

});
