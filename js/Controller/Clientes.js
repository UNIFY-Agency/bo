$('.del-btn').click( function(){
  //console.log($(this).parent().parent().parent());

  var row = $(this).parent().parent().parent();

  var client_id = row.find("td:nth-child(1)").html();
  var client_name = row.find("td:nth-child(2)").html();
  //console.log(client_id);

  $('.del-client-id').html('#' + client_id);
  $('.del-client-name').html(client_name);

  $('.del-client-link').attr("href", URL_BASE + "/clientes/excluir/" + client_id);

  $('input[name=del-client-id]').val(client_id);
  $("#del-client-form").attr('action', URL_BASE + "/clientes/listar/del");

});

$('.view-btn').click( function(){
  //console.log($(this).parent().parent().parent());

  var row = $(this).parent().parent().parent();

  var order_id = row.find("td:nth-child(1)").html();
  var order_ref = row.find("td:nth-child(2)").html();
  var order_date = row.find("td:nth-child(3)").html();
  var order_time = row.find("td:nth-child(4)").html();
  //console.log(order_id);

  $('.view-order-id').html('#' + order_id);
  $('.view-order-ref').html('#' + order_ref);

  if(order_id){
    $.post( URL_BASE + '/pedidos/detalhe', {order_id}, function( result ) {
        console.log(result);
        var results = jQuery.parseJSON(result);
        if(results[0] == 0) {
            $(".notifications").removeClass().addClass('alert alert-danger').html(results[1]).fadeIn();
        }

        var cart_itens = jQuery.parseJSON(results[1]);
        console.log(cart_itens);

        if(cart_itens[0]['cp_taxa']){
          var taxa_entrega = cart_itens[0]['cp_taxa'];
        } else {
          var taxa_entrega = cart_itens[0]['loja_taxa'];
        }

        var fatura = cart_itens[0]['fatura'] == 1 ? 'sim.    NIF: '+cart_itens[0]['nif']  : 'não' ;
        var tipo_pag = cart_itens[0]['tipo_pag'] == 2 ? 'dinheiro, troco para '+cart_itens[0]['troco']  : 'Cartão' ;

        $('.view-client-name').html(cart_itens[0]['user_name']);
        $('.view-order-time').html(order_time);
        $('.view-order-date').html(order_date);
        $('.view-order-location').html(cart_itens[0]['codigo_postal']);
        $('.view-order-endereco').html(cart_itens[0]['endereco_comp']);
        $('.view-order-fatura').html(fatura);
        $('.view-order-pag').html(tipo_pag);
        $('.view-order-contato').html(cart_itens[0]['tel2']);
        $('.view-order-email').html(cart_itens[0]['email']);

        var total_cart = 0;
        var quant_cart = 0;
        $('.cart-list').html('');
        for (var i = 0; i < cart_itens.length; i++) {
          if(cart_itens[i]['combo'] == 1){
            var cart_line = '<li class="combo_item"><a><span class="message">'+cart_itens[i]['product_name']+' ('+cart_itens[i]['quantidade']+')</span></a></li>';
          } else {
            var cart_line = '<li><a><span class="image"><img src="'+cart_itens[i]['image_url']+'" alt="img" /></span><span><span>'+cart_itens[i]['preco']+'</span><span class="time">X'+cart_itens[i]['quantidade']+'</span></span><span class="message">'+cart_itens[i]['product_name']+'</span></a></li>';
            total_cart = (parseFloat(total_cart) + parseFloat(cart_itens[i]['quantidade'] * cart_itens[i]['preco'])).toFixed(2);
            quant_cart = quant_cart + parseInt(cart_itens[i]['quantidade']);
          }
          $('.cart-list').append(cart_line);
        }

        if(total_cart >= 10){
          taxa_entrega = 'grátis';
          var total_order = parseFloat(total_cart).toFixed(2);
        } else {
          var total_order = (parseFloat(total_cart) + parseFloat(taxa_entrega)).toFixed(2);
          taxa_entrega = parseFloat(taxa_entrega).toFixed(2) + '€';
        }

        $('.view-order-quant').html(quant_cart);
        $('.view-order-subtotal').html(total_cart);
        $('.view-order-entrega').html(taxa_entrega);
        $('.view-order-total').html(total_order);
        //console.log(total_cart);
    });
  } else {
    $(".notifications").removeClass().addClass('alert alert-danger').html('<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> O sistema não conseguiu buscar o id do pedido.').fadeIn();
  }


});
