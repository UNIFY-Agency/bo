$(document).on('click', '.inat-btn', function(){

  var btn = $(this);
  var row = $(this).parent().parent().parent();
  var id = row.find("td:nth-child(1)").html();

  $.post( URL_BASE + '/cupom/inativar', {id}, function( result ) {
      console.log(result);
      $(btn).find('i').removeClass();
      $(btn).find('i').addClass('fa fa-check');
      $(btn).removeClass();
      $(btn).addClass('btn btn-success ativ-btn');
  });

});

$(document).on('click', '.ativ-btn', function(){

  var btn = $(this);
  var row = $(this).parent().parent().parent();
  var id = row.find("td:nth-child(1)").html();

  $.post( URL_BASE + '/cupom/ativar', {id}, function( result ) {
      console.log(result);
      $(btn).find('i').removeClass();
      $(btn).find('i').addClass('fa fa-close');
      $(btn).removeClass();
      $(btn).addClass('btn btn-danger inat-btn');
  });

});
