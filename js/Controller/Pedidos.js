$('.reflesh-btn').click( function(){
  location.reload();
});

$('.cancel-order').click( function(){
  var id_btn = "#cancel-btn-" + $('.view-order-id').html().substr(1);
  $('.del-client-id').html($('.view-order-id').html());
  $('.del-client-name').html($('.view-order-ref').html());
  $('input[name=del-client-id]').val($('.view-order-id').html().substr(1));
  //console.log(id_btn);
  //$(id_btn).click();
  jQuery(id_btn).trigger("click");
});

$('.del-btn').click( function(){
  //console.log($(this).parent().parent().parent());

  var row = $(this).parent().parent().parent();

  // var client_id = row.find("td:nth-child(1)").html();
  // var client_name = row.find("td:nth-child(2)").html();
  var client_id = $('.view-order-id').html();
  var client_name = $('.view-order-ref').html();
  //console.log(client_id);

  $('.del-client-id').html($('.view-order-id').html());
  $('.del-client-name').html($('.view-order-ref').html());
  //
  // $('.del-client-link').attr("href", URL_BASE + "/pedidos/cancelar/" + client_id);
  //
  // $('input[name=del-client-id]').val(client_id);
  //$("#del-client-form").attr('action', URL_BASE + "/clientes/listar/del");

});

$('.view-btn').click( function(){
  //console.log($(this).parent().parent().parent());
  $('.cart-list').empty();

  var row = $(this).parent().parent().parent();

  var order_id = row.find("td:nth-child(1)").html();
  var order_ref = row.find("td:nth-child(2)").html();
  var order_val = parseFloat(row.find("td:nth-child(4)").html());
  var order_date = row.find("td:nth-child(5)").html();
  var order_time = row.find("td:nth-child(6)").html();
  //console.log(parseFloat(order_val));

  $('.view-order-id').html('#' + order_id);
  $('.view-order-ref').html('#' + order_ref);

  if(order_id){
    $.post( URL_BASE + '/pedidos/detalhe', {order_id}, function( result ) {
        console.log(result);
        var results = jQuery.parseJSON(result);
        if(results[0] == 0) {
            $(".notifications").removeClass().addClass('alert alert-danger').html(results[1]).fadeIn();
        }

        var cupom = jQuery.parseJSON(results[2]);
        console.log(cupom);

        if (cupom['nome'] !== undefined) {
          if (cupom['tipo'] == 1) { $('.view-cupom').html('CUPOM: ' + cupom['nome'] + ' - Desconto de ' + cupom['valor'] + '%'); } else
          if (cupom['tipo'] == 2) { $('.view-cupom').html('CUPOM: ' + cupom['nome'] + ' - Desconto de ' + cupom['valor'] + '€'); }
          else { $('.view-cupom').html('CUPOM: ' + cupom['nome'] + ' - Inserção do produto ' + cupom['nome_produto']); }
          $('.view-cupom').removeClass('hidden');
        } else {
          $('.view-cupom').addClass('hidden');
        }

        var cart_itens = jQuery.parseJSON(results[1]);
        console.log(cart_itens);

        if(cart_itens[0]['cp_taxa']){
          var taxa_entrega = cart_itens[0]['cp_taxa'];
        } else {
          var taxa_entrega = cart_itens[0]['loja_taxa'];
        }

        if (cart_itens[0]['alergias'] !== null) {
          $('.view-alergias').html('ALERGIAS: ' + cart_itens[0]['alergias']);
          $('.view-alergias').removeClass('hidden');
        } else {
          $('.view-alergias').addClass('hidden');
        }

        switch(true) {
          case cart_itens[0]['entrega'] == 1:
            var tipo_entrega = 'Delivery' ;
            break;
          case cart_itens[0]['entrega'] == 2:
            var tipo_entrega = 'Take Away';
            break;
          case cart_itens[0]['entrega'] == 3:
            var tipo_entrega = 'Adiantamento Delivery para ' + cart_itens[0]['adiantado'] ;
            break;
          case cart_itens[0]['entrega'] == 4:
            var tipo_entrega = 'Adiantamento Take Away para ' + cart_itens[0]['adiantado'];
            break;
          default:
            var tipo_entrega = cart_itens[0]['entrega'];
        }

        var fatura = cart_itens[0]['fatura'] == 1 ? 'sim.    NIF: '+cart_itens[0]['nif']  : 'não' ;
        var tipo_pag = cart_itens[0]['tipo_pag'] == 2 ? 'dinheiro, troco para '+cart_itens[0]['troco']  : 'Cartão' ;
        var subtotal = parseFloat(parseFloat(cart_itens[0]['total_pedido']) - parseFloat(cart_itens[0]['taxa_entrega'])).toFixed(2);
        var total_order = parseFloat(cart_itens[0]['total_pedido']).toFixed(2);

        $('.view-client-name').html(cart_itens[0]['user_name']);
        $('.view-order-time').html(order_time);
        $('.view-order-date').html(order_date);
        $('.view-order-location').html(cart_itens[0]['codigo_postal']);
        $('.view-order-endereco').html(cart_itens[0]['endereco_comp']);
        $('.view-order-fatura').html(fatura);
        $('.view-order-pag').html(tipo_pag);
        $('.view-order-tipo-entrega').html(tipo_entrega);
        $('.view-order-contato').html(cart_itens[0]['tel2']);
        $('.view-order-email').html(cart_itens[0]['email']);

        var total_cart = 0;
        var quant_cart = 0;
        $('.cart-list').html('');
        for (var i = 0; i < cart_itens.length; i++) {
          if(cart_itens[i]['combo'] == 1){
            var cart_line = '<li class="combo_item"><a><span class="message">'+cart_itens[i]['product_name']+' ('+cart_itens[i]['quantidade']+')</span></a></li>';
          } else {
            var cart_line = '<li><a><span class="image"><img src="'+cart_itens[i]['image_url']+'" alt="img" /></span><span><span class="">'+cart_itens[i]['preco']+'</span>€<span class="time">X'+cart_itens[i]['quantidade']+'</span></span><span class="message">'+cart_itens[i]['product_name']+'</span></a></li>';
            total_cart = (parseFloat(total_cart) + parseFloat(cart_itens[i]['quantidade'] * cart_itens[i]['preco'])).toFixed(2);
            quant_cart = quant_cart + parseInt(cart_itens[i]['quantidade']);
          }
          $('.cart-list').append(cart_line);
        }

        var taxa_entrega = parseFloat(cart_itens[0]['taxa_entrega']).toFixed(2);

        if(taxa_entrega == 0.00){
          taxa_entrega = 'grátis';
        }

        // if(total_cart == order_val){
        //   taxa_entrega = 'grátis';
        //   var total_order = parseFloat(total_cart).toFixed(2);
        // } else {
        //   var total_order = (parseFloat(total_cart) + parseFloat(taxa_entrega)).toFixed(2);
        //   //taxa_entrega = parseFloat(taxa_entrega).toFixed(2) + '€';
        //   taxa_entrega = parseFloat(order_val - total_cart).toFixed(2) + '€';
        // }

        // var total_order = (parseFloat(total_cart) + parseFloat(taxa_entrega)).toFixed(2);
        // taxa_entrega = parseFloat(taxa_entrega).toFixed(2) + '€';

        $('.view-order-quant').html(quant_cart);
        //$('.view-order-subtotal').html(total_cart);
        $('.view-order-subtotal').html(subtotal);
        $('.view-order-entrega').html(taxa_entrega);
        //$('.view-order-total').html(total_order);
        $('.view-order-total').html(total_order);
        //console.log(total_cart);
    });
  } else {
    $(".notifications").removeClass().addClass('alert alert-danger').html('<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> O sistema não conseguiu buscar o id do pedido.').fadeIn();
  }


});

$('.advance-order').click( function(){

  var order_id = $('.view-order-id').html() == "" ? $(this).attr('pid') : $('.view-order-id').html().substr(1);
  var status = $(this).attr('next');
  var action = $(this).attr('action');

  var dados = [];
  dados.push({name: "order_id", value: order_id});
  dados.push({name: "status", value: status});
  //console.log(dados);

  if(dados){
    $.post( URL_BASE + '/pedidos/change', {dados}, function( result ) {
        //console.log(result);
        var results = jQuery.parseJSON(result);
        if(results[0] == 0) {
            $(".notifications").removeClass().addClass('alert alert-danger').html(results[1]).fadeIn();
          } else {
              $(".notifications").removeClass().addClass('alert alert-success').html(results[1]).fadeIn().delay(2000).fadeOut();
              window.location.replace(URL_BASE + '/pedidos/' + action);
          }

    });
  } else {
    $(".notifications").removeClass().addClass('alert alert-danger').html('<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> O sistema não conseguiu buscar o id do pedido.').fadeIn();
  }


});

$("#cancel_order_form").on('submit',(function(e) {
    e.preventDefault();

    var dados = $("#cancel_order_form").serializeArray();
     console.log(dados);

    if(dados){
        $.post( URL_BASE + '/pedidos/cancelar', {dados}, function( result ) {
                //console.log(result);
                var results = jQuery.parseJSON(result);
                if(results[0] == 0) {
                    $("#notifications").removeClass().addClass('alert alert-danger').html(results[1]).fadeIn().delay(5000).fadeOut();
                    $('#cancel_order_form')[0].reset();
                } else {
                    $("#notifications").removeClass().addClass('alert alert-success').html(results[1]).fadeIn().delay(2000).fadeOut();
                    location.reload();
                }
        });
    } else {
        $("#notifications").removeClass().html('Não há dados.').addClass('alert alert-danger').fadeIn().delay(1000).fadeOut();
    }
}));

$('.order_automatic_advance_toggle').click( function(){

    var loja = $(this).attr('loja');

    if(loja){
      $.post( URL_BASE + '/pedidos/order_automatic_advance_toggle', {loja}, function( result ) {
          console.log(result);
          var results = jQuery.parseJSON(result);
          if(results[0] == 0) {
              $(".notifications").removeClass().addClass('alert alert-danger').html(results[1]).fadeIn();
            } else {
                $(".notifications").removeClass().addClass('alert alert-success').html(results[1]).fadeIn().delay(2000).fadeOut();
                location.reload();
            }

      });
    } else {
      $(".notifications").removeClass().addClass('alert alert-danger').html('<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> O sistema não conseguiu buscar o id da loja.').fadeIn();
    }


});


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
var lastOrderId = getLastOrderId(LOJA_ID);
//lastOrderId = 158;
console.log(lastOrderId);
console.log(LOJA_ID);

var ajax_call = function() {
  console.log('passou 1min');
  var queryOrderId = getLastOrderId(LOJA_ID);
  if(lastOrderId !== queryOrderId){
    console.log('diferentes , atual: '+queryOrderId+', anteior: '+lastOrderId);
    var audio = new Audio(URL_BASE + '/assets/sound/alarm.mp3');
    audio.play();
    //document.getElementById("alarm").play();
  } else {
    console.log('iguais , atual: '+queryOrderId+', anteior: '+lastOrderId);
  }
};

var interval = 1000 * 60 * 1; // where X is your every X minutes

setInterval(ajax_call, interval);

function getLastOrderId(loja_id) {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': URL_BASE + '/pedidos/last_order_id',
        'data': { 'loja': loja_id},
        'success': function (data) {
            tmp = data;
        }
    });
    return tmp;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
