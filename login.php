<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Omnie Intranet | </title>

    <!-- Bootstrap -->
    <link href="<?php echo URL_BASE; ?>/assets/vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo URL_BASE; ?>/assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo URL_BASE; ?>/assets/vendor/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo URL_BASE; ?>/assets/vendor/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo URL_BASE; ?>/assets/css/custom.css" rel="stylesheet">
  </head>

  <body class="login">

  <?php if($mensagem_erro): ?>

    <div class="alert alert-danger text-center" style="max-width: 80%; margin: 0 auto;">
      <i class="fa fa-exclamation-triangle"></i> <?php echo $mensagem_erro; ?>
    </div>

  <?php endif; ?>

    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form id="login_form" action="" method="post">
              <h1>Omnie</h1>
              <div>
                <input type="text" name="usuario" class="form-control" placeholder="E-mail" required="" />
              </div>
              <div>
                <input type="password" name="senha" class="form-control" placeholder="Senha" required="" />
              </div>
              <div>
                <a class="btn btn-default submit" href="#" onClick="document.getElementById('login_form').submit();">Entrar</a>
                <a class="reset_pass" href="#">Esqueci minha senha.</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>

                <div>
                  <img src="<?php echo URL_BASE; ?>/assets/images/logo.jpg" />
                  <p>©2020 Todos os direitos reservados. Omnie.</p>
                </div>
              </div>
            </form>
          </section>
        </div>

        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <form>
              <h1>Create Account</h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input type="email" class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <a class="btn btn-default submit" href="index.html">Submit</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Already a member ?
                  <a href="#signin" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> Omnie</h1>
                  <p>©2020 All Rights Reserved. Omnie</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
