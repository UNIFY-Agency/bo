<?php
class GeoIp
{
    protected $api = 'https://ipapi.co/%s/json/?key=AyawnfcXcXKDIgXrJ4nh3cs76YRifwXVM2srrFOwdGXJdO5R8f';

    protected $properties = [];

    //------------------------------------------------------
    /**
    *   __get
    *
    * @access public
    * - @param $key string
    * - @return string / bool
    */
    public function __get($key)
    {
        if( isset($this->properties[$key]))
        {
            return $this->properties[$key];
        }

        return null;
    }

    //------------------------------------------------------
    /**
    *   request_geoip
    *
    * @access public
    * - @return void
    */
    public function request_geoip($ip)
    {
        $url = sprintf($this->api, $ip);
        $data = $this->send_geoip_request($url);
        $this->properties =  json_decode($data, true);
        //var_dump($this->properties);
    }

    //------------------------------------------------------
    /**
    *   send_geoip_request
    *
    * @access public
    * - @param $url string
    * - @return json
    */
    public function send_geoip_request($url)
    {
        $loc_content = file_get_contents($url);

        return $loc_content;
    }

    public function getIP(){

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
          $ip = $_SERVER['HTTP_CLIENT_IP'];}
          elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
          $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
          $ip = $_SERVER['REMOTE_ADDR'];
        }

    }
}

?>
