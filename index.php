<?php
/**
 * Created by PhpStorm.
 * User: renan
 * Date: 13/08/16
 * Time: 14:30
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

setlocale(LC_ALL, 'Portuguese_Portugal.1252');
date_default_timezone_set('Europe/Lisbon');
header('Content-Type: text/html; charset=utf8');

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', pathinfo(__FILE__)['dirname']);

include 'lib/Loader.php';

$loader = Loader::get_instance(ROOT, DS);
$loader->get('lib/Request');
$loader->get('lib/GeoIp');
$loader->get('lib/PHPMailer/src/Exception');
$loader->get('lib/PHPMailer/src/PHPMailer');
$loader->get('lib/PHPMailer/src/SMTP');

$loader->get('config/Config');
$loader->get('config/Route');
$loader->get('src/Model/Resources');
$loader->get('src/Model/Users');
$resources = new Resources();
$users = new Users(new Config());

$protocol = (empty($_SERVER['HTTPS'])) ? 'http' : 'https';

$rota = Route::dynamicUrl($protocol . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 5);
$url_base = array_pop($rota);
define("URL_BASE", $url_base);
define("SITE_URL_BASE", substr($url_base, 0, strlen($url_base) - 4));

$module = (empty($rota[0])) ? 'Inicio' : ucfirst($rota[0]);
$action = (isset($rota[1]) && (!empty($rota[1]))) ? $rota[1] : 'index';
$param = (isset($rota[2])) ? $rota[2] : null ;

if($module !== 'Login'){
  $users->protege();
}

// echo URL_BASE.'<br>';
// print_r($rota);
//
// echo '<pre>';
// print_r($_SESSION);
// echo '</pre>';
//
// $geo = new GeoIp;
// $geo->request_geoip($geo->getIP());
// echo $geo->country;

if(!empty($rota[0])){
  if (file_exists('src'.DS.'Model'.DS.$module . '.php')) { include 'src'.DS.'Model'.DS.$module . '.php'; }
  if (file_exists('src'.DS.'Controller'.DS.$module . '.php')) { include 'src'.DS.'Controller'.DS.$module . '.php'; }
}

if (!Request::isAjax()){
  if($module !== 'Login'){
    include_once 'src'.DS.'View'.DS.'view_build.php';
  } else {
    include_once 'login.php';
  }
}
