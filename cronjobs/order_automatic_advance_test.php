<?php

/*
Criado por Renan Pantoja Vilas
20-02-2020
Cronjob para avanço de pedidos
executado uma vez a cada 5 minutos
shell line: /usr/local/bin/php /home/raoonline/public_html/Admin/cronjobs/order_automatic_advance.php
*/

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

setlocale(LC_ALL, 'Portuguese_Portugal.1252');
date_default_timezone_set('Europe/Lisbon');
header('Content-Type: text/html; charset=utf8');

//chama os arquivos necessários para executar o código
set_include_path('/home/raoonline/public_html/Admin/config/');
require 'Config.php';
set_include_path('/home/raoonline/public_html/Admin/src/Model/');
require 'Lojas.php';
$lojas = new Lojas(new Config());
require 'Pedidos.php';
$orders = new Pedidos(new Config());


//pega todas as lojas que configuraram o estado automatico
$lojas_automatic_array = $lojas->getAllLojasWithAutomaticAdvance();
//verifica se há alguma loja no estado automatico se não houver para o código
if(empty($lojas_automatic_array)){ exit(); }
//coloca o id da loja como chave no array de lojas com estado automatico
foreach($lojas_automatic_array as $lojas_automatic_item){
    $lojas_automatic[$lojas_automatic_item['id_loja']] = $lojas_automatic_item;
}
//cria uma string com o id de todas as lojas que estao em estado automatico para buscar suas encomendas
$lojas_ids = implode(',', array_column($lojas_automatic, 'id_loja'));
$orders_automatic = $orders->getOrderByStoresCronAutoAdvance($lojas_ids);
//testa se há encomendas para processar se não para o código
if(empty($orders_automatic)){ exit(); }
//processa cada encomenda individualmente
foreach($orders_automatic as $order_automatic){
  $order_time = $order_automatic['atualizado'] == '0000-00-00 00:00:00' ? strtotime($order_automatic['criado']) : strtotime($order_automatic['atualizado']) ;

  //verifica se já passou os minutos configurados na tabela avanco_automatico_pedidos
  if( strtotime(date('Y-m-d H:i')) - $order_time > $lojas_automatic[$order_automatic['id_loja']]['minutos']*60 ) {
   echo 'passou '.$lojas_automatic[$order_automatic['id_loja']]['minutos'].' min <br>';
   //muda de status as encomendas da loja ate o status finalizado
   //$orders->changeOrderStatus($order_automatic['id'],$order_automatic['status']+1, null, 'cronjob');
  }
}


echo '<pre>';
print_r($lojas_automatic);
echo '</pre>';

echo $lojas_ids;

echo '<pre>';
print_r($orders_automatic);
echo '</pre>';

///home/raoonline/public_html
//echo '<br>tudo bem ate agora';

?>
